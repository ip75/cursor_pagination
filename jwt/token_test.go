package jwt

import (
	"fmt"
	"git.fintechru.org/dfa/dfa_lib/jwt/claims"
	"git.fintechru.org/dfa/dfa_lib/jwt/domain"
	"git.fintechru.org/dfa/dfa_lib/jwt/providers/url"
	"git.fintechru.org/dfa/dfa_lib/jwt/scopes"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
	"git.fintechru.org/dfa/dfa_lib/jwt/fixtures"
	"git.fintechru.org/dfa/dfa_lib/jwt/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestDifferentKeys(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	keyProvider := url.NewUrlKeyProvider(url.WithTrustedURLs([]string{
		"http://localhost:28080",
		"http://localhost:28080/api/certificates",
	}))

	r := gin.Default()
	r.GET("/api/certificates/:cert_id/", func(context *gin.Context) {
		id := context.Param("cert_id")
		fmt.Println("cert request: id=", id)
		switch id {
		case fixtures.FakeKeyID:
			context.String(http.StatusOK, fixtures.FakePublicKey)
		case fixtures.FakeKeyID2:
			context.String(http.StatusOK, fixtures.FakePublicKey2)
		}
	})
	r.GET("/test/", func(context *gin.Context) {
		jwtToken := context.GetHeader("JWT")

		token, err := NewFromString(keyProvider, jwtToken,
			WithAudience("https://dltru.org"))
		assert.NoError(t, err)

		context.Set("jwt", *token)
	})

	go func() {
		err := r.Run(":28080")
		require.NoError(t, err)
	}()

	time.Sleep(1 * time.Second)

	keyProvider.SetID(fixtures.FakeKeyID)
	keyProvider.SetPath("")
	keyProvider.SetPrivate(fixtures.FakePrivateKey)

	fakeAccessClaims := fixtures.FakeExtendedAccessClaims(fixtures.FakeUUID)
	token, err := NewWithClaims(keyProvider, fakeAccessClaims, WithKeyURL("http://localhost:28080/api/certificates"), WithIssuer("http://localhost:28080"))
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	req, err := http.NewRequest(http.MethodGet, "/test/", nil)
	req.Header.Set("JWT", token.JwtToken())
	assert.NoError(t, err)

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	assert.Equal(t, w.Code, http.StatusOK)

	keyProvider.SetID(fixtures.FakeKeyID2)
	keyProvider.SetPath("")
	keyProvider.SetPrivate(fixtures.FakePrivateKey2)

	fakeAccessClaims = fixtures.FakeExtendedAccessClaims("6b971157-2816-465e-9dad-0b5b46e70f39")
	token2, err := NewWithClaims(keyProvider, fakeAccessClaims, WithKeyURL("http://localhost:28080/api/certificates"), WithIssuer("http://localhost:28080"))
	assert.NoError(t, err)
	assert.NotEmpty(t, token2)

	req, err = http.NewRequest(http.MethodGet, "/test/", nil)
	req.Header.Set("JWT", token2.JwtToken())
	assert.NoError(t, err)

	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	assert.Equal(t, w.Code, http.StatusOK)

	req, err = http.NewRequest(http.MethodGet, "/test/", nil)
	req.Header.Set("JWT", token.JwtToken())
	assert.NoError(t, err)

	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	assert.Equal(t, w.Code, http.StatusOK)

	keyProvider.SetID(fixtures.FakeKeyID2)
	keyProvider.SetPath("")
	keyProvider.SetPrivate(fixtures.FakePrivateKey2)

	wg := sync.WaitGroup{}

	ts := time.Now()
	iterations := int32(0)
	for i := 0; i < 10_000; i++ {
		wg.Add(1)
		go func() {
			fakeAccessClaims := fixtures.FakeExtendedAccessClaims(fixtures.FakeUUID)
			token, err := NewWithClaims(keyProvider, fakeAccessClaims, WithKeyURL("http://localhost:28080/api/certificates"), WithIssuer("http://localhost:28080"))
			assert.NoError(t, err)
			assert.NotEmpty(t, token)

			req, err := http.NewRequest(http.MethodGet, "/test/", nil)
			req.Header.Set("JWT", token.JwtToken())
			assert.NoError(t, err)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			assert.Equal(t, w.Code, http.StatusOK)

			atomic.AddInt32(&iterations, 1)
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Printf("%d requests at rate %d/sec\n", iterations, iterations/int32(time.Since(ts).Seconds()))
}

func TestToken(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	fakePrivateKey := fixtures.FakeRSAPrivateKey()

	keyProvider := mocks.NewMockKeyProvider(ctrl)
	keyProvider.EXPECT().GetID().Times(6).Return(fixtures.FakeKeyID)
	keyProvider.EXPECT().GetPrivate().Times(3).Return(fakePrivateKey, nil)
	keyProvider.EXPECT().GetPath().Times(3).Return("")

	fakeAccessClaims := fixtures.FakeExtendedAccessClaims()
	fakeRefreshClaims := fixtures.FakeExtendedRefreshClaims()

	token, err := NewWithClaims(keyProvider, fakeAccessClaims)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	assert.NotEmpty(t, token.JwtToken())

	fmt.Println("-----", token.JwtToken())

	token, err = NewWithClaims(keyProvider, fakeRefreshClaims)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	assert.NotEmpty(t, token.JwtToken())

	fmt.Println("-----", token.JwtToken())

	token, err = NewWithClaims(keyProvider, fakeAccessClaims,
		WithSigningMethod(rsa.SigningMethodRS384),
	)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	assert.NotEmpty(t, token.JwtToken())

	fmt.Println("-----", token.JwtToken())
}

func TestMiddleware(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	keyProvider := mocks.NewMockKeyProvider(ctrl)
	keyProvider.EXPECT().GetID().Times(2).Return(fixtures.FakeKeyID)
	fakePrivateKey := fixtures.FakeRSAPrivateKey()
	keyProvider.EXPECT().GetPrivate().Times(1).Return(fakePrivateKey, nil)
	keyProvider.EXPECT().GetPath().Times(1).Return("")

	fakeAccessClaims := claims.NewExtendedClaims(
		claims.WithSub(fixtures.FakeUUID),
		claims.WithID(uuid.NewString()),
		claims.WithExpiresAt(time.Now().Add(10*time.Minute)),
		claims.WithScope(strings.Join([]string{domain.ScopeClient}, " ")),
	)

	token, err := NewWithClaims(keyProvider, *fakeAccessClaims)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	acl, err := scopes.ReadFromACLFile("acl.txt")
	assert.NoError(t, err)
	assert.NotNil(t, acl)

	/* ACL struct example
	structACL := []scopes.ScopeEndpoint{
			{
				Scopes: domain.ScopeClients,
				Rule:   scopes.ScopeActionAccept,
			},
			{
				Endpoint: "/api/v1/account/", // optional
				Method:   http.MethodGet,	// optional
				Scopes:   []string{domain.ScopeAccountReadAll},
				Rule:     scopes.ScopeActionAccept, // optional
			},
			{
				Endpoint: "/api/v1/account/:id/",
				Method:   http.MethodGet,
				Scopes:   []string{domain.ScopeAccountReadAll},
				Rule:     scopes.ScopeActionAccept,
			},
			{
				Endpoint: "/api/v1/account/:id/",
				Method:   http.MethodPut,
				Scopes:   []string{domain.ScopeAccountWriteAll},
				Rule:     scopes.ScopeActionAccept,
			},
		}
	*/

	mw := scopes.NewScopesMiddleware(
		scopes.WithPolicy(scopes.ScopePolicyDeny),
		scopes.WithACL(acl),
	)

	gin.SetMode(gin.TestMode)

	{
		r := gin.Default()
		r.Use(func(context *gin.Context) {
			context.Set("claims", *claims.NewExtendedClaims(
				claims.WithSub(fixtures.FakeUUID),
				claims.WithID(uuid.NewString()),
				claims.WithExpiresAt(time.Now().Add(10*time.Minute)),
				claims.WithScope(strings.Join([]string{domain.ScopeClient}, " ")),
			))
		})
		r.GET("/api/v1/account/", mw.VerifyACL())
		r.GET("/api/v1/account/:id/", mw.VerifyACL())
		r.PUT("/api/v1/account/:id/", mw.VerifyACL())
		r.GET("/api/v1/deal/", mw.VerifyACL())

		req, err := http.NewRequest(http.MethodGet, "/api/v1/account/", nil)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodPut, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/deal/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)
	}

	{
		r := gin.Default()
		r.Use(func(context *gin.Context) {
			context.Set("claims", *claims.NewExtendedClaims(
				claims.WithSub(fixtures.FakeUUID),
				claims.WithID(uuid.NewString()),
				claims.WithExpiresAt(time.Now().Add(10*time.Minute)),
				claims.WithScope(strings.Join([]string{domain.ScopeClientPartlyBlocked}, " ")),
			))
		})
		r.POST("/api/v1/dfa/", mw.VerifyACL())
		r.GET("/api/v1/account/", mw.VerifyACL())
		r.GET("/api/v1/account/:id/", mw.VerifyACL())
		r.PUT("/api/v1/account/:id/", mw.VerifyACL())
		r.GET("/api/v1/deal/", mw.VerifyACL())

		req, err := http.NewRequest(http.MethodPost, "/api/v1/dfa/", nil)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusForbidden)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/account/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodPut, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/deal/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)
	}

	{
		r := gin.Default()
		r.Use(func(context *gin.Context) {
			context.Set("claims", *claims.NewExtendedClaims(
				claims.WithSub(fixtures.FakeUUID),
				claims.WithID(uuid.NewString()),
				claims.WithExpiresAt(time.Now().Add(10*time.Minute)),
				claims.WithScope(strings.Join([]string{domain.ScopeAccountReadAll}, " ")),
			))
		})
		r.GET("/api/v1/account/", mw.VerifyACL())
		r.GET("/api/v1/account/:id/", mw.VerifyACL())
		r.PUT("/api/v1/account/:id/", mw.VerifyACL())
		r.GET("/api/v1/deal/", mw.VerifyACL())
		r.GET("/api/v1/wallet/", mw.VerifyACL())

		req, err := http.NewRequest(http.MethodGet, "/api/v1/account/", nil)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodPut, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusForbidden)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/deal/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusForbidden)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/wallet/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)
	}

	{
		r := gin.Default()
		r.Use(func(context *gin.Context) {
			context.Set("claims", *claims.NewExtendedClaims(
				claims.WithSub(fixtures.FakeUUID),
				claims.WithID(uuid.NewString()),
				claims.WithExpiresAt(time.Now().Add(10*time.Minute)),
				claims.WithScope(strings.Join([]string{domain.ScopeAccountReadAll, domain.ScopeAccountWriteAll}, " ")),
			))
		})
		r.GET("/api/v1/account/", mw.VerifyACL())
		r.GET("/api/v1/account/:id/", mw.VerifyACL())
		r.PUT("/api/v1/account/:id/", mw.VerifyACL())
		r.GET("/api/v1/deal/", mw.VerifyACL())

		req, err := http.NewRequest(http.MethodGet, "/api/v1/account/", nil)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodPut, "/api/v1/account/1/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusOK)

		req, err = http.NewRequest(http.MethodGet, "/api/v1/deal/", nil)
		assert.NoError(t, err)

		w = httptest.NewRecorder()
		r.ServeHTTP(w, req)
		assert.Equal(t, w.Code, http.StatusForbidden)
	}
}
