package claims

import (
	"encoding/json"
	"errors"
	"time"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
)

// MapClaims this is the default claims type if you don't supply one
type MapClaims map[string]interface{}

// Get returns MapClaims
func (m MapClaims) Get() interface{} {
	return m
}

// VerifyExpiresAt compares the exp claim against cmp.
func (m MapClaims) VerifyExpiresAt(cmp int64, req bool) bool {
	switch exp := m["exp"].(type) {
	case float64:
		return verifyExp(int64(exp), cmp, req)
	case json.Number:
		v, _ := exp.Int64()
		return verifyExp(v, cmp, req)
	}
	return !req
}

// VerifyNotBefore compares the nbf claim against cmp.
func (m MapClaims) VerifyNotBefore(cmp int64, req bool) bool {
	switch nbf := m["nbf"].(type) {
	case float64:
		return verifyNbf(int64(nbf), cmp, req)
	case json.Number:
		v, _ := nbf.Int64()
		return verifyNbf(v, cmp, req)
	}
	return !req
}

// Valid validates time based claims "exp, iat, nbf".
func (m MapClaims) Valid() error {
	vErr := new(rsa.ValidationError)
	now := time.Now().Unix()

	if !m.VerifyExpiresAt(now, false) {
		vErr.Inner = errors.New("token is expired")
		vErr.Errors |= rsa.ValidationErrorExpired
	}

	if !m.VerifyNotBefore(now, false) {
		vErr.Inner = errors.New("token is not valid yet")
		vErr.Errors |= rsa.ValidationErrorNotValidYet
	}

	if vErr.Valid() {
		return nil
	}

	return vErr
}
