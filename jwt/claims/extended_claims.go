package claims

import (
	"context"
	"fmt"
	"git.fintechru.org/dfa/dfa_lib/jwt/domain"
	"strings"
	"time"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
	"github.com/google/uuid"
)

// Defaults for ExtendedClaims
const (
	DefaultIssuer     = "https://dltru.org"
	DefaultAudience   = "https://dltru.org"
	DefaultExpireTime = time.Minute * 10
)

// ExtendedClaims is a structure that represents RFC7519 JWT token claims
type ExtendedClaims struct {
	StandardClaims
	LegacyClaims
	Scope string `json:"scope"`
}

// ClaimOption is a claim option
type ClaimOption func(claims *ExtendedClaims)

// NewExtendedClaims creates new ExtendedClaims
func NewExtendedClaims(opts ...ClaimOption) *ExtendedClaims {
	extendedClaims := &ExtendedClaims{
		StandardClaims: StandardClaims{
			Issuer:    DefaultIssuer,
			Audience:  DefaultAudience,
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(DefaultExpireTime).Unix(),
			ID:        uuid.NewString(),
		},
	}

	for _, opt := range opts {
		opt(extendedClaims)
	}

	return extendedClaims
}

// WithSub sets sub field
func WithSub(sub string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.Subject = sub
		claims.LegacyClaims.UUID = sub
	}
}

// WithIssuer sets iss field
func WithIssuer(issuer string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.Issuer = issuer
	}
}

// WithAudience sets aud field
func WithAudience(audience string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.Audience = audience
	}
}

// WithID sets jti field
func WithID(id string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.ID = id
	}
}

// WithExpiresAt sets exp field
func WithExpiresAt(t time.Time) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.ExpiresAt = t.Unix()
	}
}

// WithRole sets Role field
func WithRole(role string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.Role = role
	}
}

// WithScope sets scope field
func WithScope(scope string) ClaimOption {
	return func(claims *ExtendedClaims) {
		claims.Scope = scope
	}
}

// Valid a valid claim.
func (e ExtendedClaims) Valid() error {
	vErr := new(rsa.ValidationError)
	now := time.Now().Unix()

	if !e.VerifyExpiresAt(now, true) {
		delta := time.Unix(now, 0).Sub(time.Unix(e.ExpiresAt, 0))
		vErr.Inner = fmt.Errorf("token is expired by %v", delta)
		vErr.Errors |= rsa.ValidationErrorExpired
	}

	if !e.VerifyIssuedAt(now, true) {
		vErr.Inner = fmt.Errorf("token issued in future")
		vErr.Errors |= rsa.ValidationErrorIssuedInFuture
	}

	if e.ID == "" {
		vErr.Inner = fmt.Errorf("token id is empty")
		vErr.Errors |= rsa.ValidationErrorEmptyID
	}

	if e.Subject == "" {
		vErr.Inner = fmt.Errorf("empty subject in token")
		vErr.Errors |= rsa.ValidationErrorEmptySubject
	}

	if vErr.Valid() {
		return nil
	}

	return vErr
}

// GetFromContext gets and casts claims from context
func GetFromContext(ctx context.Context) (*ExtendedClaims, error) {
	c := ctx.Value("claims")
	if c == nil {
		return nil, fmt.Errorf("can't get claims from context: nil")
	}
	claims, ok := c.(ExtendedClaims)
	if !ok {
		return nil, fmt.Errorf("can't get claims from context: cast error")
	}
	return &claims, nil
}

// Get returns ExtendedClaims
func (e ExtendedClaims) Get() interface{} {
	return ExtendedClaims{
		StandardClaims: StandardClaims{
			Issuer:    e.Issuer,
			Audience:  e.Audience,
			Subject:   e.Subject,
			ID:        e.ID,
			IssuedAt:  e.IssuedAt,
			ExpiresAt: e.ExpiresAt,
		},
		LegacyClaims: LegacyClaims{
			UUID:        e.UUID,
			Role:        e.Role,
			Permissions: e.Permissions,
		},
		Scope: e.Scope,
	}
}

// GetRole gets role for service DEPRECATED, returns no error if claim has super, client* scopes
func (e ExtendedClaims) GetRole(service string, role string) (interface{}, error) {

	if scope, ok := domain.RoleScopeMap[role]; ok && e.HasScopes([]string{scope}) {
		return nil, nil
	}

	return nil, fmt.Errorf("no scope for %s", role)
}

// HasScopes checks if claims has provided scopes (all of them)
func (e ExtendedClaims) HasScopes(scopes []string) bool {
	scopesFound := 0
	for _, s := range scopes {
		for _, claimsScope := range strings.Split(e.Scope, " ") {
			if s == claimsScope {
				scopesFound++
			}
		}
	}
	if scopesFound == len(scopes) {
		return true
	}
	return false
}

// HasScope checks if claims has provided scopes (one of)
func (e ExtendedClaims) HasScope(scopes []string) bool {
	for _, s := range scopes {
		for _, claimsScope := range strings.Split(e.Scope, " ") {
			if s == claimsScope {
				return true
			}
		}
	}
	return false
}
