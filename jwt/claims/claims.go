package claims

import (
	"fmt"
	"time"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
)

// Roles is a structure for roles in scope
type Roles map[string]interface{}

// Scope is a structure for role system scope
type Scope map[string]Roles

// StandardClaims structured version of Claims
type StandardClaims struct {
	Issuer    string `json:"iss,omitempty"`
	Audience  string `json:"aud,omitempty"`
	Subject   string `json:"sub,omitempty"`
	ID        string `json:"jti,omitempty"`
	IssuedAt  int64  `json:"iat,omitempty"`
	ExpiresAt int64  `json:"exp,omitempty"`
}

// LegacyClaims is a legacy claims structure for backward compatibility
type LegacyClaims struct {
	UUID        string `json:"uuid,omitempty"`
	Role        string `json:"role,omitempty"`
	Permissions string `json:"permissions,omitempty"`
	RoleScope   Scope  `json:"role_scope,omitempty"`
}

// Get returns claims structure
func (c StandardClaims) Get() interface{} {
	return StandardClaims{
		Issuer:    c.Issuer,
		Audience:  c.Audience,
		Subject:   c.Subject,
		ID:        c.ID,
		IssuedAt:  c.IssuedAt,
		ExpiresAt: c.ExpiresAt,
	}
}

// Valid a valid claim.
func (c StandardClaims) Valid() error {
	vErr := new(rsa.ValidationError)
	now := time.Now().Unix()

	if !c.VerifyExpiresAt(now, true) {
		delta := time.Unix(now, 0).Sub(time.Unix(c.ExpiresAt, 0))
		vErr.Inner = fmt.Errorf("token is expired by %v", delta)
		vErr.Errors |= rsa.ValidationErrorExpired
	}

	if !c.VerifyIssuedAt(now, true) {
		vErr.Inner = fmt.Errorf("token issued in future")
		vErr.Errors |= rsa.ValidationErrorIssuedInFuture
	}

	if c.ID == "" {
		vErr.Inner = fmt.Errorf("token id is empty")
		vErr.Errors |= rsa.ValidationErrorEmptyID
	}

	if c.Subject == "" {
		vErr.Inner = fmt.Errorf("empty subject in token")
		vErr.Errors |= rsa.ValidationErrorEmptySubject
	}

	if vErr.Valid() {
		return nil
	}

	return vErr
}

// VerifyExpiresAt compares the exp claim against cmp.
func (c *StandardClaims) VerifyExpiresAt(cmp int64, req bool) bool {
	return verifyExp(c.ExpiresAt, cmp, req)
}

// VerifyIssuedAt verifies that claim is not issued in future
func (c *StandardClaims) VerifyIssuedAt(cmp int64, req bool) bool {
	return verifyIss(c.IssuedAt, cmp, req)
}

func verifyExp(exp int64, now int64, required bool) bool {
	if exp == 0 {
		return !required
	}
	return now <= exp
}

func verifyNbf(nbf int64, now int64, required bool) bool {
	if nbf == 0 {
		return !required
	}
	return now >= nbf
}

func verifyIss(iss int64, now int64, required bool) bool {
	if iss == 0 {
		return !required
	}
	return now >= iss
}

func verifyScope(scope Scope, required bool) bool {
	if scope == nil {
		return !required
	}
	return true
}
