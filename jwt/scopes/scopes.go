package scopes

import (
	"bufio"
	"encoding/json"
	"fmt"
	"git.fintechru.org/dfa/dfa_lib/jwt/claims"
	"git.fintechru.org/dfa/dfa_lib/logger"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"
)

const (
	ScopeActionAccept = "ACCEPT"
	ScopeActionDeny   = "DENY"

	ScopePolicyAccept = "ACCEPT"
	ScopePolicyDeny   = "DENY"
)

type ScopeAction string

type ScopeEndpoint struct {
	Endpoint string
	Scopes   []string
	Method   string
	Rule     ScopeAction
	re       *regexp.Regexp
}

type ScopeOption func(scopes *Scopes)

type Scopes struct {
	log       logger.Logger
	policy    ScopeAction
	endpoints []ScopeEndpoint
}

func WithLogger(log logger.Logger) ScopeOption {
	return func(scopes *Scopes) {
		scopes.log = log
	}
}

func ReadFromACLFile(fname string) ([]ScopeEndpoint, error) {
	f, err := os.Open(fname)
	defer f.Close()
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(f)
	acl := []ScopeEndpoint{}
	re := regexp.MustCompile("\\s|,")
	for scanner.Scan() {
		parsed := strings.Split(scanner.Text(), ";")
		if len(parsed) < 3 {
			continue
		}
		scopes := re.Split(parsed[2], -1)
		rule := ScopeActionAccept
		if len(parsed) == 4 {
			rule = parsed[3]
		}
		acl = append(acl, ScopeEndpoint{
			Method:   parsed[0],
			Endpoint: parsed[1],
			Scopes:   scopes,
			Rule:     ScopeAction(rule),
		})
	}

	return acl, nil
}

func ReadACLJsonFile(fname string) ([]ScopeEndpoint, error) {
	f, err := os.Open(fname)
	defer f.Close()
	if err != nil {
		return nil, err
	}

	b, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	var acl []ScopeEndpoint
	err = json.Unmarshal(b, &acl)
	if err != nil {
		return nil, err
	}

	return acl, nil
}

func WithACL(endpoints []ScopeEndpoint) ScopeOption {
	return func(scopes *Scopes) {
		items := []ScopeEndpoint{}
		re := regexp.MustCompile("\\/\\:.*\\/")
		for _, e := range endpoints {
			pattern := strings.ReplaceAll(e.Endpoint, "/", "\\/")
			pattern = re.ReplaceAllString(pattern, "/.*/")
			pattern = fmt.Sprintf("^%s$", pattern)
			e.re = regexp.MustCompile(pattern)
			items = append(items, e)
		}
		scopes.endpoints = items
	}
}

func WithPolicy(policy ScopeAction) ScopeOption {
	return func(scopes *Scopes) {
		scopes.policy = policy
	}
}

func NewScopesMiddleware(opts ...ScopeOption) *Scopes {
	scopes := &Scopes{
		policy:    ScopePolicyAccept,
		endpoints: []ScopeEndpoint{},
	}

	for _, opt := range opts {
		opt(scopes)
	}

	return scopes
}

func (ths *Scopes) checkScopes(
	c *gin.Context,
	scopes []string,
) bool {
	if len(scopes) == 1 && len(scopes[0]) == 0 {
		return true
	}

	extendedClaims, err := claims.GetFromContext(c)
	if err != nil {
		return false
	}

	claimsScopes := strings.Split(extendedClaims.Scope, " ")
	for _, reqScope := range scopes {
		for _, claimsScope := range claimsScopes {
			if reqScope == claimsScope {
				return true
			}
		}
	}
	return false
}

func (ths *Scopes) checkEndpoint(endpoint ScopeEndpoint, requestEndpoint string) bool {
	if len(endpoint.Endpoint) > 0 {
		return endpoint.re.MatchString(requestEndpoint)
	}
	return true
}

func (ths *Scopes) checkMethod(method, requestMethod string) bool {
	if len(method) > 0 {
		return method == requestMethod
	}
	return true
}

func (ths *Scopes) VerifyACL() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.RequestURI
		if len(path) == 0 {
			path = c.Request.URL.Path
		}

		for _, e := range ths.endpoints {
			if ths.checkEndpoint(e, path) &&
				ths.checkMethod(e.Method, c.Request.Method) &&
				ths.checkScopes(c, e.Scopes) {
				switch e.Rule {
				case "", ScopeActionAccept:
					c.Next()
					return
				case ScopeActionDeny:
					if ths.log != nil {
						ths.log.Error("Forbidden for scopes: %v", e.Scopes)
					}
					c.Status(http.StatusForbidden)
					c.Abort()
					return
				}
			}
		}
		if ths.policy == ScopeActionAccept {
			c.Next()
		}

		if ths.log != nil {
			ths.log.Error("Forbidden by policy: ", ths.policy)
		}
		c.Status(http.StatusForbidden)
		c.Abort()
	}
}
