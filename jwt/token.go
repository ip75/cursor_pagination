package jwt

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
	"git.fintechru.org/dfa/dfa_lib/jwt/claims"
	"git.fintechru.org/dfa/dfa_lib/jwt/interfaces"
)

// Token defaults
const (
	DefaultAudience = "https://dltru.org"
	DefaultIssuer   = "https://dltru.org"

	AnyIssuer   = ""
	AnyAudience = ""
)

var (
	// DefaultSigningMethod is a default signing method for new token
	DefaultSigningMethod = rsa.SigningMethodRS256
)

// Token a JWT Token.  Different fields will be used depending on whether you're
// creating or parsing/verifying a token.
type Token struct {
	keyProvider interfaces.KeyProvider
	strToken    string
	Raw         string                 // The raw token.  Populated when you Parse a token
	Method      rsa.SigningMethod      // The signing method used or to be used
	Header      map[string]interface{} // The first segment of the token
	Claims      interfaces.Claims      // The second segment of the token
	Signature   string                 // The third segment of the token.  Populated when you Parse a token
	Valid       bool                   // Is the token valid?  Populated when you Parse/Verify a token
	jku         string
	aud         string
	iss         string
}

// TokenOption is a token option
type TokenOption func(token *Token)

// WithKeyURL sets jku field
func WithKeyURL(url string) TokenOption {
	return func(token *Token) {
		if token.Header == nil {
			token.Header = map[string]interface{}{}
		}
		token.Header["jku"] = fmt.Sprintf("%s/%s", url, token.keyProvider.GetID())
	}
}

// WithAudience sets aud field
func WithAudience(audience string) TokenOption {
	return func(token *Token) {
		token.aud = audience
	}
}

// WithIssuer sets iss field
func WithIssuer(issuer string) TokenOption {
	return func(token *Token) {
		token.iss = issuer
	}
}

// WithSigningMethod option for token
func WithSigningMethod(method rsa.SigningMethod) TokenOption {
	return func(token *Token) {
		if token.Header == nil {
			token.Header = map[string]interface{}{}
		}
		token.Header["alg"] = method.Alg()
		token.Method = method
	}
}

// NewWithClaims creates a new token with provided Claims
func NewWithClaims(keyProvider interfaces.KeyProvider, claims interfaces.Claims, opts ...TokenOption) (*Token, error) {

	token := &Token{
		keyProvider: keyProvider,
		Header: map[string]interface{}{
			"typ": "JWT",
			"alg": DefaultSigningMethod.Alg(),
			"kid": keyProvider.GetID(),
			"jku": fmt.Sprintf("%s/%s", keyProvider.GetPath(), keyProvider.GetID()),
		},
		Claims: claims,
		Method: DefaultSigningMethod,
	}

	for _, opt := range opts {
		opt(token)
	}

	key, err := keyProvider.GetPrivate()
	if err != nil {
		return nil, fmt.Errorf("can't get private key: %w", err)
	}
	str, err := token.SignedString(key)
	if err != nil {
		return nil, fmt.Errorf("can't sign token: %w", err)
	}
	token.strToken = str

	return token, nil
}

// NewFromString gets JWT token from string, validates and decodes
func NewFromString(keyProvider interfaces.KeyProvider, token string, opts ...TokenOption) (*Token, error) {

	parts := strings.Split(token, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("invalid token structure")
	}

	header, err := DecodeSegment(parts[0])
	if err != nil {
		return nil, fmt.Errorf("can't decode segment 0: %w", err)
	}
	var jwtHeader map[string]interface{}
	err = json.Unmarshal(header, &jwtHeader)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal header: %w", err)
	}

	kid, ok := jwtHeader["kid"]
	if ok {
		keyID, ok := kid.(string)
		if !ok {
			return nil, fmt.Errorf("wrong format of key id")
		}

		if keyID != "" && (keyProvider.GetID() == "" || keyProvider.GetID() != keyID) {
			keyProvider.SetID(keyID)
		}

		if keyProvider.GetID() == "" {
			return nil, fmt.Errorf("no key id provided in token or set before")
		}
	}

	jku, ok := jwtHeader["jku"]
	if ok {
		keyPath, ok := jku.(string)
		if !ok {
			return nil, fmt.Errorf("wrong format of key path")
		}

		if keyPath != "" && (keyProvider.GetPath() == "" || keyProvider.GetPath() != keyPath) {
			keyProvider.SetPath(keyPath)
		}

		if keyProvider.GetPath() == "" {
			return nil, fmt.Errorf("no key path provided in token or set before")
		}
	}

	parsedKeyPub, err := keyProvider.GetPublic()
	if err != nil {
		return nil, fmt.Errorf("can't get public key: %w", err)
	}

	signingMethod := DefaultSigningMethod
	alg, ok := jwtHeader["alg"].(string)
	if ok {
		switch alg {
		case "RS256":
			signingMethod = rsa.SigningMethodRS256
		case "RS384":
			signingMethod = rsa.SigningMethodRS384
		}
	}

	newToken := &Token{
		keyProvider: keyProvider,
		Method:      signingMethod,
		Header:      jwtHeader,
		iss:         DefaultIssuer,
		aud:         DefaultAudience,
	}

	for _, opt := range opts {
		opt(newToken)
	}

	segment, err := DecodeSegment(parts[2])
	if err != nil {
		return nil, err
	}
	err = newToken.Method.Verify(strings.Join(parts[0:2], "."), segment, parsedKeyPub)
	if err != nil {
		return nil, fmt.Errorf("token verification error: %w", err)
	}

	jsonJwtBlob, err := ExportJwtData(token)
	if err != nil {
		return nil, fmt.Errorf("can't extract payload: %w", err)
	}

	extendedClaims := claims.ExtendedClaims{}
	err = json.Unmarshal(jsonJwtBlob, &extendedClaims)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal claims: %w", err)
	}

	err = extendedClaims.Valid()
	if err != nil {
		return nil, fmt.Errorf("claims not valid: %w", err)
	}

	if newToken.aud != AnyAudience && extendedClaims.Audience != newToken.aud {
		return nil, fmt.Errorf("invalid audience: %s:%s", extendedClaims.Audience, newToken.aud)
	}

	if newToken.iss != AnyIssuer && extendedClaims.Issuer != newToken.iss {
		return nil, fmt.Errorf("untrusted issuer: %s:%s", extendedClaims.Issuer, newToken.iss)
	}

	newToken.Claims = extendedClaims

	return newToken, nil
}

// GetExtendedClaims returns ExtendedClaims from token
func (t *Token) GetExtendedClaims() (*claims.ExtendedClaims, error) {
	c, ok := t.Claims.(claims.ExtendedClaims)
	if !ok {
		return nil, fmt.Errorf("can't cast claims")
	}
	return &c, nil
}

// GetFromContext gets and casts token from context
func GetFromContext(ctx context.Context) (*Token, error) {
	c := ctx.Value("jwt")
	if c == nil {
		return nil, fmt.Errorf("can't get token from context: nil")
	}
	token, ok := c.(Token)
	if !ok {
		return nil, fmt.Errorf("can't get token from context: cast error")
	}
	return &token, nil
}

// JwtToken returns a string representation of JWT token
func (t *Token) JwtToken() string {
	return t.strToken
}

// SignedString get the complete, signed token
func (t *Token) SignedString(key interface{}) (string, error) {
	var (
		sig    []byte
		sigStr string
		err    error
	)
	if sigStr, err = t.SigningString(); err != nil {
		return "", fmt.Errorf("can't get signing string: %w", err)
	}
	if sig, err = t.Method.Sign(sigStr, key); err != nil {
		return "", fmt.Errorf("can't sign: %w", err)
	}
	return strings.Join([]string{sigStr, EncodeSegment(sig)}, "."), nil
}

// SigningString generate the signing string.
func (t *Token) SigningString() (string, error) {
	var err error
	parts := make([]string, 2)
	for i := range parts {
		var jsonValue []byte
		if i == 0 {
			if jsonValue, err = json.Marshal(t.Header); err != nil {
				return "", fmt.Errorf("can't marshal header: %w", err)
			}
		} else {
			if jsonValue, err = json.Marshal(t.Claims); err != nil {
				return "", fmt.Errorf("can't marshal claims: %w", err)
			}
		}

		parts[i] = EncodeSegment(jsonValue)
	}
	return strings.Join(parts, "."), nil
}

// ExportJwtData exported data from Jwt token
func ExportJwtData(token string) ([]byte, error) {
	splitJwt := strings.Split(token, ".")
	if len(splitJwt) < 3 {
		return nil, fmt.Errorf("token segments len < 3")
	}
	decodeJwtData, err := DecodeSegment(splitJwt[1])
	if err != nil {
		return nil, fmt.Errorf("can't decode base64: %w", err)
	}
	return decodeJwtData, nil
}

// EncodeSegment encode JWT specific base64url encoding with padding stripped
func EncodeSegment(seg []byte) string {
	return strings.TrimRight(base64.URLEncoding.EncodeToString(seg), "=")
}

// DecodeSegment decode JWT specific base64url encoding with padding stripped
func DecodeSegment(seg string) ([]byte, error) {
	if l := len(seg) % 4; l > 0 {
		seg += strings.Repeat("=", 4-l)
	}

	return base64.URLEncoding.DecodeString(seg)
}
