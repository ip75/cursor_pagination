package middleware

import (
	"net/http"
	"regexp"
	"strings"

	"git.fintechru.org/dfa/dfa_lib/crypto/rsa"
	"git.fintechru.org/dfa/dfa_lib/jwt"
	"git.fintechru.org/dfa/dfa_lib/jwt/interfaces"
	"git.fintechru.org/dfa/dfa_lib/logger"
	"github.com/gin-gonic/gin"
)

const (
	DefaultValidateURL = "http://auth:19081/auth/v1/validate"
	DefaultAudience    = "https://dltru.org"
)

type AuthMiddlewareOption func(middleware *AuthMiddleware)

type AuthEndpoint struct {
	Condition      string
	Method         string
	CheckSession   bool
	BypassSession  []AuthEndpoint
	SigningMethods []rsa.SigningMethodRSA
	re             *regexp.Regexp
}

// AuthMiddleware is a middleware authenticator
type AuthMiddleware struct {
	keyProvider interfaces.KeyProvider
	log         logger.Logger
	audience    string
	validateUrl string
	endpoints   []AuthEndpoint
}

// NewAuthMiddleware creates a new Auth
func NewAuthMiddleware(
	keyProvider interfaces.KeyProvider,
	opts ...AuthMiddlewareOption,
) *AuthMiddleware {

	auth := &AuthMiddleware{
		keyProvider: keyProvider,
		endpoints:   []AuthEndpoint{},
		validateUrl: DefaultValidateURL,
		audience:    DefaultAudience,
	}

	for _, opt := range opts {
		opt(auth)
	}

	for i, e := range auth.endpoints {
		e.re = regexp.MustCompile(e.Condition)
		for j, b := range e.BypassSession {
			b.re = regexp.MustCompile(b.Condition)
			e.BypassSession[j] = b
		}
		auth.endpoints[i] = e
	}

	return auth
}

func WithLogger(log logger.Logger) AuthMiddlewareOption {
	return func(middleware *AuthMiddleware) {
		middleware.log = log
	}
}

func WithValidateURL(url string) AuthMiddlewareOption {
	return func(middleware *AuthMiddleware) {
		middleware.validateUrl = url
	}
}

func WithAudience(audience string) AuthMiddlewareOption {
	return func(middleware *AuthMiddleware) {
		middleware.audience = audience
	}
}

func WithCustomEndpoints(e []AuthEndpoint) AuthMiddlewareOption {
	return func(middleware *AuthMiddleware) {
		middleware.endpoints = e
	}
}

func (ths *AuthMiddleware) checkEndpoint(uri string, method string) (AuthEndpoint, bool) {
	for _, e := range ths.endpoints {
		if e.re.MatchString(uri) {
			if e.Method == method || method == http.MethodOptions {
				return e, true
			}
		}
	}
	return AuthEndpoint{}, false
}

// VerifyToken is a middleware for validating JWT token
func (ths *AuthMiddleware) VerifyToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			token *jwt.Token
			err   error
		)

		var jwtToken string

		// ищем стандартный заголовок авторизации
		authHeader := c.GetHeader("Authorization")
		if len(authHeader) > 0 {
			s := strings.Split(authHeader, "Bearer ")
			if len(s) == 2 {
				jwtToken = s[1]
			}
		}

		// если токена нет - пробуем получить из заголовка JWT (legacy)
		if jwtToken == "" {
			jwtToken = c.GetHeader("jwt")
		}

		ths.keyProvider.SetID("")
		ths.keyProvider.SetPath("")

		e, isMapped := ths.checkEndpoint(c.Request.RequestURI, c.Request.Method)

		if isMapped {
			if e.SigningMethods == nil {
				c.Next()
				return
			}
			for _, m := range e.SigningMethods {
				token, err = jwt.NewFromString(ths.keyProvider, jwtToken,
					jwt.WithAudience(ths.audience),
					jwt.WithSigningMethod(&m))
				if err == nil {
					break
				}
			}
		} else {
			token, err = jwt.NewFromString(ths.keyProvider, jwtToken,
				jwt.WithAudience(ths.audience))
		}

		if err != nil {
			if ths.log != nil {
				ths.log.Error("VerifyJwt error: %s", err)
			}
			c.Status(http.StatusForbidden)
			c.Abort()
			return
		}

		extendedClaims, err := token.GetExtendedClaims()
		if err != nil {
			if ths.log != nil {
				ths.log.Error("Error parsing claims")
			}
			c.Status(http.StatusForbidden)
			c.Abort()
			return
		}

		c.Set("claims", *extendedClaims)
		c.Set("token", jwtToken)
		c.Set("jwt", *token)

		c.Next()
	}
}

// VerifySession is a middleware for validating user session
func (ths *AuthMiddleware) VerifySession() gin.HandlerFunc {
	return func(c *gin.Context) {
		e, isMapped := ths.checkEndpoint(c.Request.RequestURI, c.Request.Method)

		if isMapped && !e.CheckSession {
			c.Next()
			return
		}

		req, err := http.NewRequest(http.MethodGet, ths.validateUrl, nil)
		if err != nil {
			c.Status(http.StatusInternalServerError)
			c.Abort()
			return
		}

		token, ok := c.Get("token")
		if !ok {
			if ths.log != nil {
				ths.log.Error("Can't get token from context")
			}
			c.Status(http.StatusInternalServerError)
			c.Abort()
			return
		}
		req.Header.Add("jwt", token.(string))
		req.Header.Add("req", "validate")
		req.Header.Add("req-method", "get")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			if ths.log != nil {
				ths.log.Error("Can't check session: %v", err)
			}
			c.Status(http.StatusInternalServerError)
			c.Abort()
			return
		}

		if resp.StatusCode != http.StatusOK {
			c.Status(http.StatusUnauthorized)
			c.Abort()
			return
		}
		c.Next()
	}
}
