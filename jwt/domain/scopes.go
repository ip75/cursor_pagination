package domain

// legacy scopes
const (
	ScopeSuper               = "super"
	ScopeClient              = "client"
	ScopeClientPartlyBlocked = "client.partly_blocked"
	ScopeClientFullBlocked   = "client.full_blocked"
	ScopeLawyer              = "lawyer"
	ScopeTeller              = "teller"
	ScopeSpec                = "spec"
	ScopeOperator            = "operator"
	ScopeLegal               = "legal"
	ScopeInfosec             = "infosec"
)

var (
	ScopeAdmins  = []string{ScopeSuper, ScopeSpec, ScopeLawyer, ScopeOperator, ScopeTeller, ScopeInfosec}
	ScopeClients = []string{ScopeClient, ScopeClientPartlyBlocked, ScopeClientFullBlocked}
)

// AML
const (
	ScopeAml = "aml"
)

// Approval
const (
	ScopeApprovalOperator = "approval.operator"
	ScopeApprovalLawyer   = "approval.lawyer"
	ScopeApprovalAml      = "approval.aml"
)

// Assets
const (
	ScopeAssetReadAll                = "asset.read.all"
	ScopeAssetWriteAll               = "asset.write.all"
	ScopeAssetStatus                 = "asset:status"
	ScopeAssetPayoffAgreementWrite   = "asset:payoff_agreement.write"
	ScopeAssetPayoffAgreementReadAll = "asset:payoff_agreement.read.all"
)

// Deals
const (
	ScopeDealReadAll = "deal.read.all"
)

// Exchange
const (
	ScopeExchangeStatus = "exchange:status"
)

const (
	ScopeOfferReadAll = "offer.read.all"
)

const (
	ScopeQualifiedInvestorsReadAll = "qualified_investors_application.read.all"
	ScopeQualifiedInvestorsWrite   = "qualified_investors_application.write"
)

const (
	ScopeWalletReadAll = "wallet.read.all"
)

const (
	ScopeEncumbrance         = "encumbrance"
	ScopeEncumbranceReadAll  = "encumbrance.read.all"
	ScopeEncumbranceWriteAll = "encumbrance.write.all"
	ScopeEncumbranceRead     = "encumbrance.read"
	ScopeEncumbranceStatus   = "encumbrance:status"
)

const (
	ScopeTransfer         = "transfer"
	ScopeTransfersCreate  = "transfers.create"
	ScopeTransfersRead    = "transfers.read"
	ScopeTransfersReadAll = "transfers.read.all"
)

const (
	ScopeInjunction         = "injunction"
	ScopeInjunctionWriteAll = "injunction.write.all"
	ScopeInjunctionReadAll  = "injunction.read.all"
	ScopeInjunctionRead     = "injunction.read"
)

const (
	ScopeAccountReadAll  = "account.read.all"
	ScopeAccountWriteAll = "account.write.all"
)

const (
	ScopeInvestorReadAll = "investor.read.all"
)

const (
	ScopeOrderReadAll   = "order.read.all"
	ScopeOrderCreateAll = "order.create.all"
	ScopeOrderStatus    = "order:status"
)

const (
	ScopePaymentReadAll   = "payment.read.all"
	ScopePaymentCreateAll = "payment.create.all"
	ScopePaymentStatus    = "payment:status"
)

const (
	ScopeBankAccountDetailsReadAll = "bank_account_details.read.all"
)

const (
	ScopeDocumentReadAll  = "document.read.all"
	ScopeDocumentWriteAll = "document.write.all"
	ScopeDocumentRead     = "document.read"
	ScopeDocumentWrite    = "document.write"
)

const (
	ScopeApplicationFormReadAll = "application_form.read.all"
)

const (
	ScopeUserReadAll  = "user.read.all"
	ScopeUserWriteAll = "user.write.all"
	ScopeUserStatus   = "user:status"
)

const (
	ScopeSession = "session"
)

const (
	ScopeEmployee         = "employee"
	ScopeEmployeeReadAll  = "employee.read.all"
	ScopeEmployeeWriteAll = "employee.write.all"
)

const (
	ScopeCertificateWrite   = "certificate.write"
	ScopeCertificateReadAll = "certificate.read.all"
)

// Номинальные счета
const (
	ScopeNominalAccountWriteAll = "nominal_account.write.all"
)

// Тарифы
const (
	// Просмотр ТП (список, деталка), Создание ТП, Архивирование ТП, история назначений ТП, текущий ТП и Базовый ТП
	ScopeTariffReadAll  = "tariff.read.all"
	ScopeTariffWriteAll = "tariff.write.all"
	ScopeTariffRead     = "tariff.read"

	// Добавление Пользователей и Выпусков в ТП
	ScopeTariffAssign = "tariff.assign"
)
