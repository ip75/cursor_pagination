package domain

// Role names
const (
	DefaultRole    = "DEFAULT"  // DefaultRole
	SuperRole      = "SUPER"    // SuperRole
	TellerRole     = "TELLER"   // TellerRole
	LawyerRole     = "LAWYER"   // LawyerRole
	SpecialistRole = "SPEC"     // SpecialistRole
	ClientRole     = "CLIENT"   // ClientRole
	OperatorRole   = "OPERATOR" // OperatorRole
	LegalRole      = "LEGAL"    // LegalRole

	ClientPartlyBlockRole = "CLIENT_PARTLY_BLOCK"
	ClientFullBlockRole   = "CLIENT_FULL_BLOCK"

	InfosecRole = "INFOSEC" // InfosecRole
)

var RoleScopeMap = map[string]string{
	SuperRole:             ScopeSuper,
	ClientRole:            ScopeClient,
	ClientPartlyBlockRole: ScopeClientPartlyBlocked,
	ClientFullBlockRole:   ScopeClientFullBlocked,
	TellerRole:            ScopeTeller,
	LawyerRole:            ScopeLawyer,
	OperatorRole:          ScopeOperator,
	SpecialistRole:        ScopeSpec,
	LegalRole:             ScopeLegal,
}
