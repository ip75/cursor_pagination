# dfa_lib

## Token

Пример проверки токена:

````
// Создать KeyProvider
keyProvider = url.NewUrlKeyProvider()

// Получить и проверить токен из строки
// Public ключ для проверки подписи токена будет взят по URL из поля jku токена
// URL проверяется на присутствие в списке доверенных, по умолчанию ["http://localhost:19081/", "https://dltru.org/api/certificates/"]
// Будут учитываться поля iss, aud - по умолчанию заданы https://dltru.org

someToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
token, err = jwt.NewFromString(&keyProvider, someToken)
if err != nil {
    log.Error("Invalid token: %v", err)
}
````

Пример получения Claims:

````
// Получить ExtendedClaims из контекста
extendedClaims, err := claims.GetFromContext(context)
if err != nil {
    log.Error("Invalid claims: %v", err)
}

// Получить ExtendedClaims из токена
extendedClaims, err := token.GetExtendedClaims()
if err != nil {
    log.Error("Invalid claims: %v", err)
}
````

Пример проверки роли в Claims:

````
// Проверить наличие роли SUPER для сервиса core
role, err := extendedClaims.GetRole("core", SUPER")
if err != nil {
    log.Error("Permission denied: %v", err)
}
````
