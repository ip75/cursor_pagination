package roles

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"git.fintechru.org/dfa/dfa_lib/jwt/claims"
	"git.fintechru.org/dfa/dfa_lib/logger"
	"github.com/gin-gonic/gin"
)

type RoleAction string

const (
	ServiceAny      = "ANY"
	RoleActionPass  = "PASS"
	RoleActionBreak = "BREAK"
)

var (
	RoleSetAdmin  = []string{"SUPER", "OPERATOR", "TELLER", "LAWYER", "SPEC"}
	RoleSetClient = []string{"CLIENT", "LEGAL"}
)

type RoleOption func(roles *Roles)

var (
	BreakAction = func(role string, val interface{}) RoleAction { return RoleActionBreak }
	PassAction  = func(role string, val interface{}) RoleAction { return RoleActionPass }
)

type RoleEndpoint struct {
	RoleSet        []string
	Method         string
	PositiveAction func(role string, val interface{}) RoleAction
	NegativeAction func(role string, val interface{}) RoleAction
	re             *regexp.Regexp
}

type RoleEndpointMap map[string]RoleEndpoint

type Roles struct {
	log       logger.Logger
	service   string
	policy    RoleAction
	endpoints RoleEndpointMap
}

func WithLogger(log logger.Logger) RoleOption {
	return func(roles *Roles) {
		roles.log = log
	}
}

func WithService(service string) RoleOption {
	return func(roles *Roles) {
		roles.service = service
	}
}

func WithEndpoints(endpoints RoleEndpointMap) RoleOption {
	return func(roles *Roles) {
		for k, v := range endpoints {
			pattern := strings.ReplaceAll(k, "/", "\\/")
			pattern = strings.ReplaceAll(pattern, ":id", ".*")
			pattern = fmt.Sprintf("^%s$", pattern)
			v.re = regexp.MustCompile(pattern)
			endpoints[k] = v
		}
		roles.endpoints = endpoints
	}
}

func WithPolicy(policy RoleAction) RoleOption {
	return func(roles *Roles) {
		roles.policy = policy
	}
}

func NewRolesMiddleware(opts ...RoleOption) *Roles {
	roles := &Roles{
		service:   ServiceAny,
		policy:    RoleActionBreak,
		endpoints: map[string]RoleEndpoint{},
	}

	for _, opt := range opts {
		opt(roles)
	}

	return roles
}

func (ths *Roles) CheckRoleAction(
	c *gin.Context,
	service string,
	roles []string,
	positiveAction func(role string, val interface{}) RoleAction,
	negativeAction ...func(role string, val interface{}) RoleAction,
) (RoleAction, error) {
	extendedClaims, err := claims.GetFromContext(c)
	if err != nil {
		return ths.policy, fmt.Errorf("can't get claims: %w", err)
	}

	for claimsService, claimsRoles := range extendedClaims.RoleScope {
		if service != ServiceAny && claimsService != service {
			continue
		}
		for role, val := range claimsRoles {
			for _, r := range roles {
				if role == r {
					if positiveAction != nil {
						return positiveAction(role, val), nil
					} else {
						return ths.policy, nil
					}
				}
			}
		}
	}

	action := ths.policy
	if len(negativeAction) > 0 {
		if negativeAction[0] != nil {
			action = negativeAction[0]("", nil)
		}
	}
	return action, nil
}

func (ths *Roles) VerifyEndpoint() gin.HandlerFunc {
	return func(c *gin.Context) {

		var endpoint *RoleEndpoint

		for _, v := range ths.endpoints {
			if v.re.MatchString(c.Request.RequestURI) && v.Method == c.Request.Method {
				endpoint = &v
				break
			}
		}
		if endpoint == nil {
			if ths.policy == RoleActionPass {
				c.Next()
				return
			}
			ths.log.Warn("Forbidden by policy: %s", c.Request.RequestURI)
			c.Status(http.StatusForbidden)
			c.Abort()
			return
		}

		action, err := ths.CheckRoleAction(c, ths.service, endpoint.RoleSet, endpoint.PositiveAction, endpoint.NegativeAction)
		if err != nil && ths.policy != RoleActionPass {
			ths.log.Error("Can't check roles: %v", err)
			c.Status(http.StatusForbidden)
			c.Abort()
			return
		}
		if action != RoleActionPass {
			ths.log.Error("Forbidden for roleset: %v", endpoint.RoleSet)
			c.Status(http.StatusForbidden)
			c.Abort()
			return
		}

		c.Next()
	}
}
