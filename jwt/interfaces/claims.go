package interfaces

//go:generate mockgen -destination=../mocks/mock_claims.go -package=mocks git.fintechru.org/dfa/dfa_lib/jwt/interfaces Claims

// Claims for a type to be a Claims object, it must just have a Valid method that determines
type Claims interface {
	Valid() error
	Get() interface{}
}
