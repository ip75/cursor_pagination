package interfaces

import "crypto/rsa"

//go:generate mockgen -destination=../mocks/mock_key_provider.go -package=mocks git.fintechru.org/dfa/dfa_lib/jwt/interfaces KeyProvider

// KeyProvider is an interface for KeyProvider
type KeyProvider interface {
	GetPrivate() (*rsa.PrivateKey, error)
	GetPublic() (*rsa.PublicKey, error)
	GetID() string
	SetID(id string)
	GetPath() string
	SetPath(path string)

	SetPrivate(key string)
}
