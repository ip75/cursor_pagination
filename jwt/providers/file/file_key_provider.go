package file

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"

	rsa2 "git.fintechru.org/dfa/dfa_lib/crypto/rsa"
)

// Consts for file provider
const (
	PrivateKeyPath = "%s/%s"
	PublicKeyPath  = "%s/%s.pub"
)

// KeyProvider is an implementation of KeyProvider for files
type KeyProvider struct {
	id      string
	path    string
	private []byte
}

// GetID returns key ID
func (ths *KeyProvider) GetID() string {
	return ths.id
}

// SetID sets key ID
func (ths *KeyProvider) SetID(id string) {
	ths.id = id
}

// GetPath gets key path
func (ths *KeyProvider) GetPath() string {
	return ths.path
}

// SetPath sets key path
func (ths *KeyProvider) SetPath(path string) {
	ths.path = path
}

// GetPrivate provides RSA private key
func (ths *KeyProvider) GetPrivate() (*rsa.PrivateKey, error) {
	key, err := ths.readPrivate()
	if err != nil {
		return nil, fmt.Errorf("can't read public key: %w", err)
	}
	parsedKey, err := rsa2.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		return nil, fmt.Errorf("can't parse RSA from PEM: %w", err)
	}
	return parsedKey, nil
}

// GetPublic provides RSA private key
func (ths *KeyProvider) GetPublic() (*rsa.PublicKey, error) {
	key, err := ths.readPublic()
	if err != nil {
		return nil, fmt.Errorf("can't read public key: %w", err)
	}
	parsedKey, err := rsa2.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		return nil, fmt.Errorf("can't parse RSA from PEM: %w", err)
	}
	return parsedKey, nil
}

// SetPrivate set private key
func (ths *KeyProvider) SetPrivate(key string) {
	ths.private = []byte(key)
}

// ReadPrivate reads private key from file
func (ths *KeyProvider) readPrivate() ([]byte, error) {
	key, err := ioutil.ReadFile(fmt.Sprintf(PrivateKeyPath, ths.path, ths.id))
	if err != nil {
		return nil, fmt.Errorf("can't read private key: %w", err)
	}
	return key, nil
}

// ReadPublic read public key from file
func (ths *KeyProvider) readPublic() ([]byte, error) {
	key, err := ioutil.ReadFile(fmt.Sprintf(PublicKeyPath, ths.path, ths.id))
	if err != nil {
		return nil, fmt.Errorf("can't read public key: %w", err)
	}
	return key, nil
}

// NewFileKeyProvider create new file key reader
func NewFileKeyProvider() *KeyProvider {
	return &KeyProvider{}
}
