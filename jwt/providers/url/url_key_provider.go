package url

import (
	"crypto/rsa"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"

	rsa2 "git.fintechru.org/dfa/dfa_lib/crypto/rsa"
)

var (
	// TrustedKeySources is a default trusted sources
	TrustedKeySources = []string{"http://localhost:19081/api/certificates/", "http://auth:19081/api/certificates/"}
)

// KeyProviderOption is a KeyProvider option
type KeyProviderOption func(keyProvider *KeyProvider)

// KeyProvider is structure for KeyProvider
type KeyProvider struct {
	sync.RWMutex
	id          string
	path        string
	private     []byte
	public      []byte
	keyCache    *keyCache
	trustedURLs []string
}

type key struct {
	id  string
	key []byte
}

type keyCache struct {
	sync.RWMutex
	m map[string]key
}

// GetID returns key ID
func (ths *KeyProvider) GetID() string {
	ths.RLock()
	defer ths.RUnlock()
	return ths.id
}

// SetID sets key ID
func (ths *KeyProvider) SetID(id string) {
	ths.Lock()
	defer ths.Unlock()
	ths.id = id
}

// SetPath sets key path
func (ths *KeyProvider) SetPath(url string) {
	ths.Lock()
	defer ths.Unlock()
	ths.path = url
}

// GetPath gets key path
func (ths *KeyProvider) GetPath() string {
	ths.RLock()
	defer ths.RUnlock()
	return ths.path
}

// GetPrivate provides RSA private key
func (ths *KeyProvider) GetPrivate() (*rsa.PrivateKey, error) {
	ths.RLock()
	defer ths.RUnlock()
	parsedKey, err := rsa2.ParseRSAPrivateKeyFromPEM(ths.private)
	if err != nil {
		return nil, fmt.Errorf("can't parse RSA from PEM: %w", err)
	}
	return parsedKey, nil
}

// GetPublic provides RSA private key
func (ths *KeyProvider) GetPublic() (*rsa.PublicKey, error) {
	ths.RLock()
	defer ths.RUnlock()
	key, err := ths.readPublic()
	if err != nil {
		return nil, fmt.Errorf("can't read public key: %w", err)
	}
	parsedKey, err := rsa2.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		return nil, fmt.Errorf("can't parse RSA from PEM: %w", err)
	}
	return parsedKey, nil
}

// SetPrivate set private key
func (ths *KeyProvider) SetPrivate(key string) {
	ths.Lock()
	defer ths.Unlock()
	ths.private = []byte(key)
}

func (ths *KeyProvider) readPrivate() ([]byte, error) {
	return ths.readFromCache(ths.getFromUrl)
}

func (ths *KeyProvider) readPublic() ([]byte, error) {
	return ths.readFromCache(ths.getFromUrl)
}

func (ths *KeyProvider) readFromCache(readerFunc func(url string) ([]byte, error)) ([]byte, error) {
	ths.keyCache.Lock()
	defer ths.keyCache.Unlock()

	if k, ok := ths.keyCache.m[ths.id]; ok {
		return k.key, nil
	}

	k, err := readerFunc(ths.path)
	if err != nil {
		return nil, fmt.Errorf("can't read key: %w", err)
	}

	if len(k) > 0 {
		ths.keyCache.m[ths.id] = key{
			id:  ths.id,
			key: k,
		}
	}

	return k, nil
}

func (ths *KeyProvider) isTrusted(url string) bool {
	for _, u := range ths.trustedURLs {
		if strings.HasPrefix(url, u) {
			return true
		}
	}

	return false
}

func (ths *KeyProvider) getFromUrl(url string) (r []byte, e error) {
	if !ths.isTrusted(url) {
		return nil, fmt.Errorf("untrusted URL: %s", url)
	}

	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("can't get key: %w", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			r = nil
			e = fmt.Errorf("can't close response: %w", err)
		}
	}(resp.Body)

	k, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("can't read key from response: %w", err)
	}

	return k, nil
}

// WithTrustedURLs sets trustedURLs field
func WithTrustedURLs(urls []string) KeyProviderOption {
	return func(keyProvider *KeyProvider) {
		keyProvider.trustedURLs = urls
	}
}

// NewUrlKeyProvider create new url key provider
func NewUrlKeyProvider(opts ...KeyProviderOption) *KeyProvider {
	cache := &keyCache{
		m: map[string]key{},
	}
	keyProvider := &KeyProvider{
		keyCache:    cache,
		trustedURLs: TrustedKeySources,
	}

	for _, opt := range opts {
		opt(keyProvider)
	}

	return keyProvider
}
