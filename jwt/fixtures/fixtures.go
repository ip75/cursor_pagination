package fixtures

import (
	"crypto/rsa"
	"git.fintechru.org/dfa/dfa_lib/jwt/domain"
	"github.com/gin-gonic/gin"
	"strings"
	"time"

	rsa2 "git.fintechru.org/dfa/dfa_lib/crypto/rsa"
	"git.fintechru.org/dfa/dfa_lib/jwt/claims"
	"github.com/google/uuid"
)

// Fake fixtures
const (
	FakeUUID = "113de0ff-8f81-4fac-a63a-4a9088dc76b8"

	FakeKeyID  = "162bca67-720a-4890-878f-53ffca69a8ef"
	FakeKeyID2 = "128f9730-607e-40f2-9ed4-9fc76990ba6f"

	FakePrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIG5AIBAAKCAYEAwspfKbauNbE8SkB1WFS3RFs6I0TAJWdiLJMs6jkbviHZGP27
8uleUanhExlASW/6V9fVRQZbd3fr3U/glkYnnwT+xPhdUhCJJKU6+TEV0ApU6sVL
Gg0uI2z6SoWNG2PbhhTrIgTrx7CbXkgK5KV4lDLEtdXsp1b5L2D13mbSxO2lg/g1
EWlAs6RsVYqHQUjdizmSDMzubnjPRAYpHhxIej+Ed5ShR68ZEH+LVQIYwxlDiZ8v
+vWuG5yN3tiKCvZN3drLl/y3YjyTHGvWqoU36f+BMP+s8l+iEyL5m0mYxwAlRd43
4JHpxctI2mqRm8MfNYSSgNI4xNOlAcnsYfNhpSmYTNeL5WV7mx0JLVdYN91HoQ1/
vj8rmm1FkbCprR5F9PZLL/mXNx43R0m5RTzneRi+SPY8GbuDo+0xYF9FQV01qz5v
ZwD3CvSNefbbo6ZPlwzIClxtArAUCU4NKr7CvgNNLacF0jdKnzr9Y6cVHvOcjNwf
YVWdksPv4uJ94OBfAgMBAAECggGAcz929GAwTHYzfJ8Mjpl44Qm0qR9JLBgx3rup
LF9jiafPIdkl9X+bnWeMUldzUL/e91Asz60GOvYJ6CQVdfPGB3DWJ8XmuOItsi/M
Z5liFXe3bDwUxjIcJTuaDrNbnGQFgxwnTjZpJfnYwvyO7V71q4EqH4eVgCCWSDDu
54oglAEjF6SoCUgjzuWubyIKCldJNNF4Fvi87UGvJ8kZVMZZH3MrNfELRzVS+A7u
afpWmj/9VJSJt8raG14SFlI5wmic+uLKsatN/aw3fSXxuJkjf0zWfMhmMO+/ODhj
l5R0S9bXjBSSWCuSGb3OfVb0529f0+Wcbuc9k1zVutBs6cMBZUvJbtpO+VmeTKa2
3EhFB0k96+nqMJbX0ReKY5KP3VasM6AEAZ/m+RiLzbef6s7BcP3Pl4Rj/bA3RE4A
64ALVnZLyLhCurBR8GUBYDIm3Fj/rpSQbCA9ew840GAtej4slHDx8aSg1vd574Ap
7OLZ8PVa4NeY0MnbXZxQlAZlT52hAoHBAOpsuME2O8lDZIBsmjZoMo0NeomtxGMx
nBzIioOfM5vt3HOGcbAWGMPo3n1EFIiPHsXlvQ14tK03Fmz0K0M/sVqMCH9QKkSH
lHGOlGTZSWXBDv/zGfKdyJS3xp7VNcU5gqnZ2OuNIbp4nZ++yZUnAOMndPrdfTXt
qYJl3kiF6wd6CCeLl8d4yGNp0IxGZU6zGQcpIFTpt32CD1UPrCoxSJiMQDCDlB+0
ICzV1aYRgFeNhnZG+JOgLcrTXufDVUzBLwKBwQDUt9RW7jQuib4bCJXxVNrlFkX/
XhbzYAOZVLlrjYE7c1c6Z+TYVp4ufv3WCJXsTsqqfUJsR+efdk9STbzhTVPEL+Gc
7NoCJx3zv56MdTPKEtaPL5OoVAoZCa8SQECMTD+oLJdVmLkB7bJicPxILrJpBrGD
C943fEulR7lkC5AtB1zfMrXn9tt0nO218zrWED4mmIxuOdIQ7imt045xOKqiqLCo
7ilH0aB803EpyFH1mVVlNHpaDA5vabwwpweHJ9ECgcEAmsW/UWlCHcZN43BQfJ1c
yr3camv8nlURsftBYFdK1qc7hlZ8VvqvzSDBTEkXmXWMprHgmLVZ7IbTWon2ykaS
DKsprQy7a1VsT3EITnEbQYAuLBRPzFwaIv4Wt6UjBBzNRODck+AxJZEe2Go5Yy1G
K7NQS5TSZzmLUdl54b3xB5WtLpa+VedLvj8Nfp+3oTcjzhN8EjPgfo/RGaLoU08I
lBb9BkrJEAl3uMJRdXxWlPeOkCaZVbZKpX/aSAAyICPnAoHAQcaKMUs89XsAPsm7
28D6YsYtZNaKUD1LP5l0MG60HFcBDP1dRQwbo5L/GLTU7xo6DtfmtybQ13007+U7
14sasNqBcA3JJ8tCtsF03EfNeNm0YNg8V1uNN8rHyOP3UaquMGmBbiVoZ9FBTLFV
QVi3MESylSwRTaTwmy3/hJESxum07Qt4Nj8eAwQXeSC5+8FQBo07N0ZHz5Zlauu0
6wuzr9JrtgDW4O9QKekT3/Cib0g3oLDGeJurxwWx/HVZnZBRAoHBAIvDzjOjMIBm
rrYmGaUCRdiydnrEO//7rsfJNHjUttxTMLkbLyQby0TJNwJUIayP602tRLdZKqCx
f/+nC2H7InS5rGXQHIycrKmLMDkHyRsxQdXV3LT4qUdloh8ju0W1Ag5nchU1E9hK
FyPdTo909we6Amn6WTf18tsaRgIVjdrgMH2e/p2rmoDJZEHxoa0EXxehmkAT0cvE
uue13Xwxm3Q0EH8NM3EXtsBvo/awm2yEQdpW+hrsW+YeWkGKepRlCg==
-----END RSA PRIVATE KEY-----`

	FakePrivateKey2 = `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA5WG6io//MMbUXYukL0UKKj2sHgGZxqzVwNkKWXmu0yvdgW24
LJJTuN8OwGU9DmNwDBOkpZofzBL24btGHt5xGF5a5vz1VmciMubPNKaXCgE8ZJBM
g85c76RKOq/aA8HEj3U1XTAs0pvajtrtIKqXKxXyNZvbMxx02UV23ZTm8BcMaVa2
sRZNqwI3fIYhgOYyjERNxsvCVPG41bV3PSY5xqvoGGAZ2inX4HDh/sv5OyG86c0m
qviaK6dzkF2ULsIdlegnYfTxboeWdX+xxgFkdo01LM9aC19rwAJm1VCd0jPJSFiL
gzJoQHXQUKaTrO/V59Nave+cPP1/UJdr0SwDmwIDAQABAoIBAHFnlMNGl+L0mi+5
hy27k1/456xaHD3GW1OhumUsLMvIZxXDOggHPA8tcGycYMhJTy8ZUxBwl4HcamSF
oxBF52Il3AlRTLYv/Q9vX0fniMrwe3w4fyu3DemIrw4+vKmY1eYbC4wL8k0Zsr9n
6054KI6rpfjtGj01t2tn1XGNWXOe8dzjWKJKxMQDlsjaSS6MYDH9eah4pMeaTbYy
v0Nuwqa5/P9GiyBc/sK0vWqWvtfaM+PX4AzWOLNa8FFNV1IC0sljCLLL04uOR+9H
H7JdxfYtC73SiGyZg/WxeNEqxSNvTKz4cT0alGEK/vGaWf07aPxWFDPtJjixX3FA
wT6dAUECgYEA+C2ISI1H8PeZVNabA+cGD0mpIdFF0J7WVpgz1QgL38Lbno//waCH
8M5eo8A7qI4vHajP8gA5LuuyMolLz/DbJ+K79nuTbuZW0N0pryWjZXPP8CVCbhpM
+XhwU6McaeWJU7/Wm/JtQgmTDxCvRhhLkHBDeMfJF0S0v4SjnU0nhzsCgYEA7JyJ
WwvIrkvLFp4KYzG+dTTUVIwAy3HlpYl0eY4vuR+IfDcqas0UaylmIIKnN3l/AkjW
LPlEAFAyiWQRFw6lLX66LdXvTmf4vfdYTT9W2+QliigDc0riEnZtj3tGieK5EzgV
GxyIPJnTsBOHTUST6iIzAJPsLWWCu8ctuZVDbyECgYEAvSIuH2SUM/7t+qrGXlX8
1z/CxoPvGctC+6Fotuw36GJe7ts9BToq/4i2tUK19IHPRGI3f21OJwT2u+7IOQOM
cTwpFPIebMDb3rb9on4jpRh6msA6fSBMEz8HE6ZV7BpC6vFjQ1ugilgzm6KSgO/Y
/o2fzfcruDDjMqWJ70IyYqsCgYAvVsJEbP8hoBhp1gTOA83PNVql77zY91iy3eVn
zILPzvdHouHhrFZBem7sYjeCadR3aIsxRE3Oojz6MAMbUdrKV2h4cy0FyteBalNI
YTvHksXP9mF6V6kTeI5C/q8GN05AAiB25CZ9uWkY+U9Ark/JsD30B6RhEMYvO5Jy
ZuKvAQKBgBiOEmw+ukrCSyhpdZ2isaeyMbwrPs5gzxMQDWodVPFemn8NH4fN/1FM
CEW8ucwYx+wDl1Q079t81nwhsJ2xcumFFyr6rQEAtWV3sXaAfIbp03qDla71iSGN
1fO3EagK2mil8gsMBzBXJXRaS7aZRCF7sT6ACMJ6hd1q4wTR93aW
-----END RSA PRIVATE KEY-----`

	FakePublicKey = `-----BEGIN PUBLIC KEY-----
MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAwspfKbauNbE8SkB1WFS3
RFs6I0TAJWdiLJMs6jkbviHZGP278uleUanhExlASW/6V9fVRQZbd3fr3U/glkYn
nwT+xPhdUhCJJKU6+TEV0ApU6sVLGg0uI2z6SoWNG2PbhhTrIgTrx7CbXkgK5KV4
lDLEtdXsp1b5L2D13mbSxO2lg/g1EWlAs6RsVYqHQUjdizmSDMzubnjPRAYpHhxI
ej+Ed5ShR68ZEH+LVQIYwxlDiZ8v+vWuG5yN3tiKCvZN3drLl/y3YjyTHGvWqoU3
6f+BMP+s8l+iEyL5m0mYxwAlRd434JHpxctI2mqRm8MfNYSSgNI4xNOlAcnsYfNh
pSmYTNeL5WV7mx0JLVdYN91HoQ1/vj8rmm1FkbCprR5F9PZLL/mXNx43R0m5RTzn
eRi+SPY8GbuDo+0xYF9FQV01qz5vZwD3CvSNefbbo6ZPlwzIClxtArAUCU4NKr7C
vgNNLacF0jdKnzr9Y6cVHvOcjNwfYVWdksPv4uJ94OBfAgMBAAE=
-----END PUBLIC KEY-----`

	FakePublicKey2 = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5WG6io//MMbUXYukL0UK
Kj2sHgGZxqzVwNkKWXmu0yvdgW24LJJTuN8OwGU9DmNwDBOkpZofzBL24btGHt5x
GF5a5vz1VmciMubPNKaXCgE8ZJBMg85c76RKOq/aA8HEj3U1XTAs0pvajtrtIKqX
KxXyNZvbMxx02UV23ZTm8BcMaVa2sRZNqwI3fIYhgOYyjERNxsvCVPG41bV3PSY5
xqvoGGAZ2inX4HDh/sv5OyG86c0mqviaK6dzkF2ULsIdlegnYfTxboeWdX+xxgFk
do01LM9aC19rwAJm1VCd0jPJSFiLgzJoQHXQUKaTrO/V59Nave+cPP1/UJdr0SwD
mwIDAQAB
-----END PUBLIC KEY-----`
)

// FakeRSAPrivateKey provides fake RSA private key
func FakeRSAPrivateKey() *rsa.PrivateKey {
	parsedKey, err := rsa2.ParseRSAPrivateKeyFromPEM([]byte(FakePrivateKey))
	if err != nil {
		panic(err)
	}
	return parsedKey
}

// FakeRSAPublicKey provides fake RSA public key
func FakeRSAPublicKey() *rsa.PublicKey {
	parsedKey, err := rsa2.ParseRSAPublicKeyFromPEM([]byte(FakePublicKey))
	if err != nil {
		panic(err)
	}
	return parsedKey
}

// FakeExtendedAccessClaims provides fake AccessClaims
func FakeExtendedAccessClaims(userUUIDs ...string) *claims.ExtendedClaims {
	userUUID := FakeUUID
	if len(userUUIDs) > 0 {
		userUUID = userUUIDs[0]
	}
	expirationTime := time.Now().Add(10 * time.Minute)
	c := claims.NewExtendedClaims(
		claims.WithSub(userUUID),
		claims.WithID(uuid.NewString()),
		claims.WithExpiresAt(expirationTime),
		claims.WithScope(strings.Join([]string{domain.ScopeAccountReadAll, domain.ScopeAccountWriteAll, domain.ScopeApplicationFormReadAll}, " ")),
	)
	err := c.Valid()
	if err != nil {
		return nil
	}
	return c
}

// FakeExtendedRefreshClaims provides fake RefreshClaims
func FakeExtendedRefreshClaims() *claims.ExtendedClaims {
	expirationTime := time.Now().Add(10 * time.Minute)
	c := claims.NewExtendedClaims(
		claims.WithID(uuid.NewString()),
		claims.WithExpiresAt(expirationTime),
	)
	err := c.Valid()
	if err != nil {
		return nil
	}
	return c
}

func FakeClaims() func(ctx *gin.Context) {
	return func(context *gin.Context) {
		context.Set("claims", *FakeExtendedAccessClaims())
	}
}
