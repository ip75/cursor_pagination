package pagination

import (
	"net/url"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCursorToken_ParseQuery(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedOrder.Encode() + "&" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewCursorToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, c.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, c.Order[i])
		}

		// Проверка фильтров поиска
		assert.Equal(t, filters, c.Filter)
	})

	t.Run("ok - wrong order", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		expectedOrders := []string{
			"asc(id)",
			"desc(create_time)",
		}

		orders := []string{
			"asc(id)",
			"desc(create_time)",
			"somewrongmodificator(id)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedOrder.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewCursorToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, c.Limit)

		// Проверка сортировки
		assert.Equal(t, len(expectedOrders), len(c.Order))
		assert.Equal(t, expectedOrders, c.Order)
	})

	t.Run("ok - wrong filter", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		expectedFilters := map[string][]string{
			"age": {"30", "gt(45)"},
		}

		filters := map[string][]string{
			"age":   {"30", "gt(45)", ""},
			"name":  {""},
			"count": {"wrongmodificator(1)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewCursorToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, uint64(10), c.Limit)

		// Проверка поискового фильтра
		assert.Equal(t, len(expectedFilters), len(c.Filter))
		assert.Equal(t, expectedFilters, c.Filter)
	})

	t.Run("ok - request with cursor", func(t *testing.T) {
		cursor := "eyJmaWx0ZXIiOnsiYWdlIjpbIjMwIiwiZ3QoMzApIl19LCJvcmRlciI6WyJkZXNjKGlkKSJdLCJsaW1pdCI6MTAsIm5leHQiOnsic29tZS1sYXN0LWVsZW1lbnQta2V5Ijoic29tZS1sYXN0LWVsZW1lbnQtdmFsdWUifX0K"
		rawUrl := "https://example.com/api/v1/users?cursor=" + cursor

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		assert.NotEmpty(t, token.Limit)
		assert.NotEmpty(t, token.Filter)
		assert.NotEmpty(t, token.Order)
	})
}

func TestCursorToken_ParseToken(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedOrder.Encode() + "&" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextData := map[string]string{
			"some-last-element-key": "some-last-element-value",
		}
		nextToken, err := token.GenerateNextToken(nextData)
		assert.NoError(t, err)
		assert.True(t, len(nextToken) > 0)

		// Парсинг токена
		parsedToken := NewCursorToken()
		err = parsedToken.ParseToken(nextToken)
		assert.NoError(t, err)

		// Проверка next данных
		assert.Equal(t, nextData, parsedToken.Next)

		// Проверка лимита
		assert.Equal(t, limit, parsedToken.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, parsedToken.Order[i])
		}

		// Проверка фильтров поиска
		assert.Equal(t, filters, parsedToken.Filter)
	})

	t.Run("ok - wrong order", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		expectedOrders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		orders := []string{
			"asc(id)",
			"desc(create_time)",
			"somewrongmodificator(id)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedOrder.Encode() + "&" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextData := map[string]string{
			"some-last-element-key": "some-last-element-value",
		}
		nextToken, err := token.GenerateNextToken(nextData)
		assert.NoError(t, err)
		assert.True(t, len(nextToken) > 0)

		// Парсинг токена
		parsedToken := NewCursorToken()
		err = parsedToken.ParseToken(nextToken)
		assert.NoError(t, err)

		// Проверка next данных
		assert.Equal(t, nextData, parsedToken.Next)

		// Проверка лимита
		assert.Equal(t, limit, parsedToken.Limit)

		// Проверка сортировки
		assert.Equal(t, len(expectedOrders), len(parsedToken.Order))
		assert.Equal(t, expectedOrders, parsedToken.Order)

		// Проверка фильтров поиска
		assert.Equal(t, filters, parsedToken.Filter)
	})

	t.Run("ok - wrong filter", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)

		expectedFilters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		filters := map[string][]string{
			"age":   {"30", "gt(45)", ""},
			"name":  {""},
			"count": {"wrongmodificator(1)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextData := map[string]string{
			"some-last-element-key": "some-last-element-value",
		}
		nextToken, err := token.GenerateNextToken(nextData)
		assert.NoError(t, err)
		assert.True(t, len(nextToken) > 0)

		// Парсинг токена
		parsedToken := NewCursorToken()
		err = parsedToken.ParseToken(nextToken)
		assert.NoError(t, err)

		// Проверка next данных
		assert.Equal(t, nextData, parsedToken.Next)

		// Проверка лимита
		assert.Equal(t, limit, parsedToken.Limit)

		// Проверка поискового фильтра
		assert.Equal(t, len(expectedFilters), len(parsedToken.Filter))
		assert.Equal(t, expectedFilters, parsedToken.Filter)
	})
}

func TestCursorToken_GeneratePrevToken(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		prevData := map[string]string{
			"some-last-element-key": "some-last-element-value",
		}
		prevToken, err := cursor.GeneratePrevToken(prevData)
		assert.NoError(t, err)
		assert.True(t, len(prevToken) > 0)

		parsedCursor := NewCursorToken()
		err = parsedCursor.ParseToken(prevToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedCursor.Limit)
		assert.NotEmpty(t, parsedCursor.Filter)
		assert.NotEmpty(t, parsedCursor.Order)
		assert.NotEmpty(t, parsedCursor.Prev)
		assert.Empty(t, parsedCursor.Next)
	})

	t.Run("error - prev is nil", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		prevToken, err := cursor.GeneratePrevToken(nil)
		assert.NoError(t, err)
		assert.Empty(t, prevToken)
	})

	t.Run("error - prev is wrong", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		prevData := map[string]string{
			"": "some-last-element-value",
		}
		prevToken, err := cursor.GeneratePrevToken(prevData)
		assert.Error(t, err)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
		assert.Empty(t, prevToken)

		// Генерируем токен
		prevData = map[string]string{
			"some-last-element-key": "",
		}
		prevToken, err = cursor.GeneratePrevToken(prevData)
		assert.Error(t, err)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
		assert.Empty(t, prevToken)
	})
}

func TestCursorToken_GenerateNextToken(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextData := map[string]string{
			"some-last-element-key": "some-last-element-value",
		}
		nextToken, err := cursor.GenerateNextToken(nextData)
		assert.NoError(t, err)
		assert.True(t, len(nextToken) > 0)

		parsedCursor := NewCursorToken()
		err = parsedCursor.ParseToken(nextToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedCursor.Limit)
		assert.NotEmpty(t, parsedCursor.Filter)
		assert.NotEmpty(t, parsedCursor.Order)
		assert.NotEmpty(t, parsedCursor.Next)
		assert.Empty(t, parsedCursor.Prev)
	})

	t.Run("error - next is nil", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextToken, err := cursor.GenerateNextToken(nil)
		assert.NoError(t, err)
		assert.Empty(t, nextToken)
	})

	t.Run("error - next is wrong", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?age=30&age=gt(30)&order=desc(id)&limit=10"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		cursor := NewCursorToken()
		err = cursor.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		nextData := map[string]string{
			"": "some-last-element-value",
		}
		nextToken, err := cursor.GeneratePrevToken(nextData)
		assert.Error(t, err)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
		assert.Empty(t, nextToken)

		// Генерируем токен
		nextData = map[string]string{
			"some-last-element-key": "",
		}
		nextToken, err = cursor.GeneratePrevToken(nextData)
		assert.Error(t, err)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
		assert.Empty(t, nextToken)
	})
}

func TestCursorToken_validatePrevNext(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		prev := map[string]string{
			"last_name": "Ivanov",
		}

		token := NewCursorToken()
		err := token.validatePrevNext(prev)
		assert.NoError(t, err)
	})

	t.Run("ok - empty data", func(t *testing.T) {
		prev := map[string]string{}

		token := NewCursorToken()
		err := token.validatePrevNext(prev)
		assert.NoError(t, err)
	})

	t.Run("ok - nil data", func(t *testing.T) {
		token := NewCursorToken()
		err := token.validatePrevNext(nil)
		assert.NoError(t, err)
	})

	t.Run("error - empty keys", func(t *testing.T) {
		prev := map[string]string{
			"last_name": "Ivanov",
			"":          "gt(39)",
		}

		token := NewCursorToken()
		err := token.validatePrevNext(prev)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
	})

	t.Run("error - empty values", func(t *testing.T) {
		prev := map[string]string{
			"last_name": "",
			"age":       "gt(39)",
		}

		token := NewCursorToken()
		err := token.validatePrevNext(prev)
		assert.ErrorIs(t, err, errPrevNextDataIsInvalid)
	})
}
