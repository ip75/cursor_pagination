package pagination

import (
	"fmt"
	"gorm.io/gorm"
)

// OrderClauseConverter создаёт условие ORDER BY для различных источников данных
type OrderClauseConverter struct {
	modificators []*orderModificator
}

// NewOrderClauseConverter конструктор для OrderClauseConverter
func NewOrderClauseConverter(modificators []string) (*OrderClauseConverter, error) {
	mm := make([]*orderModificator, 0, len(modificators))
	for _, mod := range modificators {
		m := newOrderModificator()
		if err := m.Parse(mod); err != nil {
			return nil, err
		}

		if err := m.Validate(); err != nil {
			return nil, err
		}

		mm = append(mm, m)
	}

	return &OrderClauseConverter{
		modificators: mm,
	}, nil
}

// ToGORM создаёт условие сортировки для GORM'a
func (c OrderClauseConverter) ToGORM(db *gorm.DB) (*gorm.DB, error) {
	for _, m := range c.modificators {
		clause := fmt.Sprintf("%s %s", m.arg, m.operator)
		db = db.Order(clause)
	}

	return db, nil
}
