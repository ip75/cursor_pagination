package pagination

import (
	"fmt"
	"math"
	"strconv"
)

// CursorAPIPResponse объект пагинации с Cursor токенами
type CursorAPIPResponse struct {
	Pagination struct {
		Cursors struct {
			Prev string `json:"prev"`
			Next string `json:"next"`
		} `json:"cursors"`
	} `json:"paging"`
}

// NewCursorAPIPResponse генерирует объект пагинации с Cursor токенами
func NewCursorAPIPResponse(token *CursorToken, prevData, nextData map[string]string) (*CursorAPIPResponse, error) {
	r := &CursorAPIPResponse{}

	// Генерируем токен на предыдущую страницу
	prevToken, err := token.GeneratePrevToken(prevData)
	if err != nil {
		return nil, err
	}

	// Генерируем токен на следующую страницу
	nextToken, err := token.GenerateNextToken(nextData)
	if err != nil {
		return nil, err
	}

	if len(prevToken) == 0 && len(nextToken) == 0 {
		return nil, fmt.Errorf("prev and next data is empty")
	}

	r.Pagination.Cursors.Prev = prevToken
	r.Pagination.Cursors.Next = nextToken

	return r, nil
}

// OffsetAPIResponse объект пагинации с Offset токенами
type OffsetAPIResponse struct {
	Pagination struct {
		Cursors map[string]string `json:"cursors"`
	} `json:"paging"`
}

// NewOffsetAPIResponse генерирует объект пагинации с Offset токенами
func NewOffsetAPIResponse(token *OffsetToken, count uint64) (*OffsetAPIResponse, error) {
	r := &OffsetAPIResponse{}
	r.Pagination.Cursors = make(map[string]string)
	totalPages := uint64(math.Ceil(float64(count) / float64(token.Limit)))

	// Генерируем токены для остальных страниц
	for i := uint64(1); i < totalPages+1; i++ {
		t, err := token.GenerateToken(i)
		if err != nil {
			return nil, err
		}
		r.Pagination.Cursors[strconv.FormatUint(i, 10)] = t
	}

	return r, nil
}
