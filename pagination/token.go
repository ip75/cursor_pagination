package pagination

import (
	"errors"
	"net/url"
	"strconv"
	"strings"
)

const (
	// limitParam наименование query-параметра для Token.Limit
	limitParam = "limit"

	// orderParam наименование query-параметров для Token.Order
	orderParam = "order"

	// pageParam наименование query-параметра для OffsetToken.Offset
	pageParam = "page"

	// cursorParam наименование query-параметра для получения токена в base64
	cursorParam = "cursor"
)

// Token - общая структура для токена пагинации
type Token struct {
	// Filter - список параметров фильтрации запроса
	Filter map[string][]string `json:"filter"`

	// Order - список параметров для сортировки результатов запроса
	Order []string `json:"order"`

	// Limit - максимальное количество возвращаемых данных
	Limit uint64 `json:"limit"`
}

// ParseQuery заполняет структуру токена из query-параметров
func (c *Token) ParseQuery(query string) error {
	// Разбираем строку GET - запроса
	params, err := url.ParseQuery(query)
	if err != nil {
		return err
	}

	// Разбор параметра limit
	limit := params.Get(limitParam)
	if len(limit) > 0 {
		c.Limit, err = strconv.ParseUint(limit, 10, 64)
		if err != nil {
			return err
		}
	}

	// Разбор параметров order
	if orders, ok := params[orderParam]; ok {
		for _, order := range orders {
			m := newOrderModificator()

			if err := m.Parse(order); err != nil {
				continue
			}

			if err := m.Validate(); err != nil {
				continue
			}

			c.Order = append(c.Order, m.String())
		}
	}

	// Разбор параметр filter (оставшиеся query-параметры, после limit и orders)
	filters := make(map[string][]string, len(params)-2)
	for name, values := range params {
		// Сохраняем остальные параметры
		if name == limitParam || name == orderParam || name == pageParam {
			continue
		}

		for _, val := range values {
			val = strings.TrimSpace(val)

			if len(val) == 0 {
				continue
			}

			m := newFilterModificator()
			if err := m.Parse(val); err != nil {
				if errors.Is(err, errModificatorIsEmpty) {
					filters[name] = append(filters[name], val)
				}

				continue
			}

			if err := m.Validate(); err != nil {
				continue
			}

			filters[name] = append(filters[name], m.String())
		}
	}
	c.Filter = filters

	return nil
}
