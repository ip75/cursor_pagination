package pagination

import (
	"errors"
	"fmt"
	re "regexp"
	"strings"
)

// Возможные операторы для сортировки
const (
	// orderAsc модификатор для ASC сортировки
	orderAsc = "asc"

	// orderDesc модификатор для DESC сортировки
	orderDesc = "desc"
)

// Возможные операторы для поиска
const (
	// filterGt - оператор "больше" (>)
	filterGt = "gt"

	// filterGe - оператор "больше или равно" (>=)
	filterGe = "ge"

	// filterLt - оператор "меньше" (<)
	filterLt = "lt"

	// filterLe - оператор "меньше или равно" (<=)
	filterLe = "le"

	// filterLike - оператор "поиска подстроки в строке"
	filterLike = "like"
)

// Ошибки при валидации модификатора
var (
	errModificatorIsEmpty   = errors.New("modificator is empty")
	errModificatorIsInvalid = errors.New("modificator is invalid")
	errOperatorIsInvalid    = errors.New("operator is invalid")
	errArgumentIsInvalid    = errors.New("argument is invalid")
)

// Ошибки при валидации модификатора сортировки
var (
	errOrderOperatorIsInvalid = errors.New("order operator is invalid")
)

// Ошибки при валидации модификатора поиска
var (
	errFilterOperatorIsInvalid = errors.New("filter operator is invalid")
)

// modificator представляет обобщённый модификатор поиска/сортировки
type modificator struct {
	// operator - оператор; в "gt(30)", оператор - "gt"
	operator string

	// arg - аргумент оператора; в "gt(30)", аргумент - "30".
	arg string
}

// Parse разбирает модификаторы поиска/сортировки
func (m *modificator) Parse(source string) error {
	pattern := `(^[a-zA-Zа-яА-Я]+)\(([\s a-zA-Zа-яА-Я0-9_-]+)\)`
	r := re.MustCompile(pattern)

	source = strings.TrimSpace(source)
	parts := r.FindStringSubmatch(source)

	if len(parts) < 3 {
		if len(parts) == 0 {
			return errModificatorIsEmpty
		}
		return fmt.Errorf("%w: modificator is invalid", errModificatorIsInvalid)
	}

	// Валидируем оператор
	op := parts[1]
	if len(op) == 0 {
		return fmt.Errorf("%w: operator is empty", errOperatorIsInvalid)
	}
	m.operator = op

	// Валидируем аргумент
	arg := parts[2]
	if len(arg) == 0 {
		return fmt.Errorf("%w: argument is empty", errArgumentIsInvalid)
	}
	m.arg = arg

	return nil
}

// String возвращает строковое представление модификатора
func (m *modificator) String() string {
	return fmt.Sprintf("%s(%s)", m.operator, m.arg)
}

// orderModificator представляет модификатор сортировки
type orderModificator struct {
	modificator
}

// newOrderModificator конструктор для orderModificator
func newOrderModificator() *orderModificator {
	return &orderModificator{}
}

// Validate валидирует модификатор сортировки
func (m orderModificator) Validate() error {
	if m.operator != orderDesc && m.operator != orderAsc {
		return fmt.Errorf("%w: need %s or %s, got: %s", errOrderOperatorIsInvalid, orderDesc, orderAsc, m.operator)
	}

	m.arg = strings.TrimSpace(strings.ToLower(m.arg))

	return nil
}

// filterModificator представляет модификатор поиска
type filterModificator struct {
	modificator
}

// newFilterModificator конструктор для filterModificator
func newFilterModificator() *filterModificator {
	return &filterModificator{}
}

// Validate валидирует модификатор поиска
func (m filterModificator) Validate() error {
	switch m.operator {
	case filterGt, filterGe, filterLt, filterLe, filterLike:
		m.arg = strings.TrimSpace(strings.ToLower(m.arg))
	default:
		return fmt.Errorf("%w: operator %s is unsuported", errFilterOperatorIsInvalid, m.operator)
	}

	m.arg = strings.TrimSpace(strings.ToLower(m.arg))

	return nil
}
