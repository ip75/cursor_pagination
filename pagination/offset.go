package pagination

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
)

// OffsetToken - тип токена при пагинации с помощью смещения
type OffsetToken struct {
	Token

	// Offset - смещение результатов поиска
	Offset uint64 `json:"offset"`
}

// NewOffsetToken конструктор для OffsetToken
func NewOffsetToken() *OffsetToken {
	return &OffsetToken{}
}

// ParseQuery заполняет структуру токена из query-параметров
func (ot *OffsetToken) ParseQuery(query string) error {
	// Разбираем строку GET - запроса
	params, err := url.ParseQuery(query)
	if err != nil {
		return err
	}

	// Получение курсора из строки запроса
	cursor := params.Get(cursorParam)
	if len(cursor) > 0 {
		if err := ot.ParseToken(cursor); err != nil {
			return err
		}
		return nil
	}

	// Курсор не был передан, разбираем строку с запросом
	if err := ot.Token.ParseQuery(query); err != nil {
		return err
	}

	// Разбор параметра page
	page := params.Get(pageParam)
	if len(page) > 0 {
		pageInt, err := strconv.ParseUint(page, 10, 64)
		if err == nil {
			ot.Offset = ot.calculateOffset(pageInt)
		}
	}

	return nil
}

// ParseToken разбирает токен в формате base64
func (ot *OffsetToken) ParseToken(encodedToken string) error {
	// Проверяем валидность токена
	if len(encodedToken) == 0 {
		return fmt.Errorf("cursor token: %w", ErrTokenIsInvalid)
	}

	token, err := base64.StdEncoding.DecodeString(encodedToken)
	if err != nil {
		return err
	}

	// Разбираем токен в структуру
	if err := json.Unmarshal(token, ot); err != nil {
		return err
	}

	return nil
}

// GenerateToken генерирует токен для конкретной страницы
func (ot *OffsetToken) GenerateToken(page uint64) (string, error) {
	t := *ot
	t.Offset = ot.calculateOffset(page)

	return t.generateBase64Token()
}

// GeneratePrevToken генерирует prev токен
func (ot *OffsetToken) GeneratePrevToken() (string, error) {
	if ot.Offset == 0 || ot.Offset < ot.Limit {
		return "", nil
	}
	t := *ot
	t.Offset = ot.Offset - ot.Limit

	return t.generateBase64Token()
}

// GenerateNextToken генерирует next токен
func (ot *OffsetToken) GenerateNextToken() (string, error) {
	t := *ot
	t.Offset = ot.Offset + ot.Limit

	return t.generateBase64Token()
}

// calculateOffset рассчитывает смещений записей в зависимости от
func (ot *OffsetToken) calculateOffset(page uint64) uint64 {
	if page == 0 {
		return 0
	}

	return (page - 1) * ot.Limit
}

// generateBase64Token генерирует токен в строку в формате base64
func (ot *OffsetToken) generateBase64Token() (string, error) {
	var buf bytes.Buffer
	encoder := base64.NewEncoder(base64.StdEncoding, &buf)
	err := json.NewEncoder(encoder).Encode(ot)
	if err != nil {
		return "", err
	}

	if err := encoder.Close(); err != nil {
		return "", nil
	}

	return buf.String(), nil
}
