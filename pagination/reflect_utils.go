package pagination

import (
	"fmt"
	"github.com/gobeam/stringy"
	"gorm.io/gorm"
	"reflect"
	"strconv"
	"strings"
)

// parseTag парсит тэг поля
func (ct *CursorToken) parseTag(tag string) map[string]string {
	result := map[string]string{}
	elements := strings.Split(tag, ";")
	for _, e := range elements {
		s := strings.Split(e, ":")
		if len(s) == 2 {
			result[s[0]] = s[1]
		}
	}
	return result
}

// getTableByStruct получает имя таблицы по структуре
func (ct *CursorToken) getTableByStruct(data interface{}) (string, error) {
	typ, err := ct.getDataType(data)
	if err != nil {
		return "", fmt.Errorf("can't get table: %w", err)
	}

	rv := reflect.New(typ)
	method := rv.MethodByName("TableName")
	if method.IsValid() {
		res := method.Call([]reflect.Value{})
		if len(res) > 0 && res[0].IsValid() {
			return res[0].String(), nil
		}
	}

	return "", fmt.Errorf("can't find method TableName")
}

// getJoinFromStruct формирует JOINS, SELECTS из структуры учитывая foreignKey
func (ct *CursorToken) getJoinFromStruct(data interface{}, field string) (string, string) {
	var join string

	// найти основную таблицу, с которой идет JOIN
	mainTable, err := ct.getTableByStruct(data)
	if err != nil {
		return "", ""
	}

	selects := []string{}

	// найти поля с foreignKey и по тэгу построить JOIN
	ct.walkFields(data, func(f reflect.StructField, val reflect.Value) {
		if f.Type.Kind() == reflect.Struct {
			if f.Name == field {
				if val.IsValid() {
					m := val.MethodByName("TableName")
					if m.IsValid() {
						res := m.Call([]reflect.Value{})
						if len(res) > 0 && res[0].IsValid() {
							table := res[0].String()
							tags := ct.parseTag(f.Tag.Get("gorm"))

							if from, ok := tags["foreignKey"]; ok {
								if to, ok := tags["references"]; ok {
									alias := f.Name
									from := stringy.New(from).SnakeCase().ToLower()
									to := stringy.New(to).SnakeCase().ToLower()
									join = fmt.Sprintf("LEFT JOIN \"%s\" \"%s\" ON \"%s\".\"%s\" = \"%s\".\"%s\"", table, alias, alias, from, mainTable, to)
									ct.walkFields(val.Interface(), func(field reflect.StructField, value reflect.Value) {
										if field.Type.Kind() != reflect.Struct {
											name := stringy.New(field.Name).SnakeCase().ToLower()
											columnAlias := fmt.Sprintf("\"%s__%s\"", alias, name)
											selects = append(selects, fmt.Sprintf("\"%s\".\"%s\" AS %s", alias, name, columnAlias))
										}
									})
								}
							}
						}
					}
				}
			}
		} else {
			name := stringy.New(f.Name).SnakeCase().ToLower()
			selects = append(selects, fmt.Sprintf("\"%s\".\"%s\"", mainTable, name))
		}
	})

	return join, strings.Join(selects, ",")
}

// getAllowedFilters отдает map из возможных полей фильтрации с их типами
// поля определяются исходя из структуры данных
func (ct *CursorToken) getAllowedFilters(data interface{}) (map[string]string, error) {
	var (
		typ reflect.Type
		v   reflect.Value
	)

	rv := reflect.ValueOf(data)
	if !rv.IsValid() {
		return nil, fmt.Errorf("can't reflect data")
	}

	// преобразуем к интерфейсу
	iface := rv.Interface()

	rv = reflect.ValueOf(iface)
	if !rv.IsValid() {
		return nil, fmt.Errorf("can't reflect interface")
	}

	// возьмем элемент из интерфейса
	elem := rv.Elem()
	if !elem.IsValid() {
		return nil, fmt.Errorf("can't get reflect elem")
	}

	// определяем тип нижележащей структуры внутри слайса
	// либо непосредственно выставляем тип структуры
	if elem.Type().Kind() == reflect.Slice {
		v = reflect.ValueOf(elem)
		typ = v.Type()
	} else if elem.Type().Kind() == reflect.Struct {
		typ = elem.Type()
	} else {
		return nil, fmt.Errorf("data is not a slice nor single struct")
	}

	if typ.Kind() == reflect.Struct {
		// создадим новый слайс такого же типа с единственным элементом
		newSlice := reflect.MakeSlice(elem.Type(), 1, 1)

		// берем тип элемента слайса
		etype := newSlice.Index(0).Type()

		allowedFilters := map[string]string{}

		// проход по видимым полям структуры
		for _, f := range reflect.VisibleFields(etype) {
			if f.Type.Kind() == reflect.Struct {
				// это embedded
				for _, embedded := range reflect.VisibleFields(f.Type) {
					// преобразуем имя поля в SnakeCase в нижнем регистре
					s := stringy.New(embedded.Name)
					switch s.Get() {
					case "String", "Int8", "Int16", "Int32", "Int64", "wall":
						s := stringy.New(f.Name)
						allowedFilters[s.SnakeCase().ToLower()] = f.Type.Name()
						break
					}
					allowedFilters[s.SnakeCase().ToLower()] = embedded.Type.Name()
				}
				continue
			}
			// преобразуем имя поля в SnakeCase в нижнем регистре
			s := stringy.New(f.Name)
			allowedFilters[s.SnakeCase().ToLower()] = f.Type.Name()
		}
		return allowedFilters, nil
	}
	return nil, nil
}

// getDataType находит тип данных под ptr
func (ct *CursorToken) getDataType(data interface{}) (reflect.Type, error) {
	rv := reflect.ValueOf(data)
	if !rv.IsValid() {
		return nil, fmt.Errorf("can't reflect data")
	}

	// преобразуем к интерфейсу
	iface := rv.Interface()

	rv = reflect.ValueOf(iface)
	if !rv.IsValid() {
		return nil, fmt.Errorf("can't reflect interface")
	}

	// возьмем элемент из интерфейса
	elem := rv.Elem()
	if !elem.IsValid() {
		return nil, fmt.Errorf("can't get reflect elem")
	}

	// определяем тип нижележащей структуры внутри слайса
	// либо непосредственно выставляем тип структуры
	if elem.Type().Kind() == reflect.Slice {
		// создадим новый слайс такого же типа с единственным элементом
		newSlice := reflect.MakeSlice(elem.Type(), 1, 1)

		// берем тип элемента слайса
		return newSlice.Index(0).Type(), nil
	} else if elem.Type().Kind() == reflect.Struct {
		return elem.Type(), nil
	} else {
		return nil, fmt.Errorf("data is not a slice nor single struct")
	}
}

// walkFields получает тип данных и походит по полям структуры
func (ct *CursorToken) walkFields(data interface{}, cb func(reflect.StructField, reflect.Value)) {

	typ, err := ct.getDataType(data)
	if err != nil {
		return
	}

	// проход по видимым полям структуры
	if typ.Kind() == reflect.Struct {
		for _, f := range reflect.VisibleFields(typ) {
			cb(f, reflect.New(f.Type))
		}
	}

	return
}

// getField ищет поле в структуре по его имени и возвращает его значение в строковом представлении
func (ct *CursorToken) getField(field string, item interface{}) (string, error) {
	r := reflect.ValueOf(item)
	if !r.IsValid() {
		return "", fmt.Errorf("reflection error on item")
	}

	var val reflect.Value

	// проход по видимым полям структуры
	for _, v := range reflect.VisibleFields(r.Type()) {

		// посмотреть в тэг json на предмет имени поля
		tag := v.Tag.Get("json")
		if strings.ToLower(tag) == strings.ToLower(field) {
			val = reflect.Indirect(r).FieldByName(v.Name)
			break
		}

		// поиск по полному совпадению имени поля
		if strings.ToLower(v.Name) == strings.ToLower(field) {
			val = reflect.Indirect(r).FieldByName(v.Name)
			break
		}

		// пробуем найти по SnakeCase
		if strings.ToLower(v.Name) == strings.ToLower(strings.ReplaceAll(field, "_", "")) {
			val = reflect.Indirect(r).FieldByName(v.Name)
			break
		}
	}

	if !val.IsValid() {
		return "", fmt.Errorf("can't find field %s", field)
	}

	// возьмем интерфейс от поля и посмотрим тип
	// в зависимости от типа - сконвертируем в строку
	switch val.Interface().(type) {
	case int, int8, int16, int32, int64:
		i := val.Int()
		return strconv.FormatInt(i, 10), nil
	case uint, uint8, uint16, uint32, uint64:
		i := val.Uint()
		return strconv.FormatUint(i, 10), nil
	case bool:
		switch val.Bool() {
		case true:
			return "true", nil
		case false:
			return "false", nil
		}
	case string:
		return val.String(), nil
	default:
		return fmt.Sprintf("%v", val), nil
	}

	return "", fmt.Errorf("can't determine type of %v", val.Interface())
}

// setData обрезает данные до limit, устанавливает результат в data и устанавливает токены пагинации
func (ct *CursorToken) setData(rv reflect.Value, data interface{}) error {
	if !rv.IsValid() {
		return nil
	}
	rvType := rv.Type()

	if data == nil || rvType.Kind() != reflect.Slice {
		return nil
	}

	hasNext := false
	hasPrev := false

	if rv.IsValid() {
		rlen := rv.Len()
		if rlen > 0 {
			// движение вперед с токеном или первый вызов без токена
			if ct.Next != nil || (ct.Next == nil && ct.Prev == nil) {

				// проверяем, есть ли следующая страница
				if ct.Limit > 0 && (uint64(rlen) > ct.Limit) {
					hasNext = true
				} else {
					hasNext = false
				}

				// в зависимости от наличия следующей страницы,
				// обрезаем результирующий слайс, чтобы не отдавать лишние строки
				if hasNext {
					var newData reflect.Value
					if rlen-1 >= 1 {
						newData = rv.Slice(0, rlen-1)
					} else {
						newData = rv.Slice(0, rlen)
					}
					dest := reflect.ValueOf(data).Elem()
					dest.Set(newData)
				} else {
					newData := rv.Slice(0, rlen)
					dest := reflect.ValueOf(data).Elem()
					dest.Set(newData)
				}

				// установка токенов Next и Prev по результирующему слайсу
				err := ct.SetTokens(rv.Slice(0, rv.Len()).Interface())
				if err != nil {
					return fmt.Errorf("can't set tokens: %w", err)
				}

				if !hasNext {
					ct.Next = nil
				}
				return nil
			}

			// движение назад с токеном prev
			if ct.Prev != nil {

				// проверяем, есть ли следующая страница
				if uint64(rlen) > ct.Limit {
					hasPrev = true
				} else {
					hasPrev = false
				}

				// в зависимости от наличия следующей страницы,
				// обрезаем результирующий слайс, чтобы не отдавать лишние строки
				if hasPrev {
					newData := rv.Slice(1, rlen)
					dest := reflect.ValueOf(data).Elem()
					dest.Set(newData)
				} else {
					newData := rv.Slice(0, rlen)
					dest := reflect.ValueOf(data).Elem()
					dest.Set(newData)
				}

				// установка токенов Next и Prev по результирующему слайсу
				err := ct.SetTokens(rv.Slice(0, rv.Len()).Interface())
				if err != nil {
					return fmt.Errorf("can't set tokens: %w", err)
				}

				if !hasPrev {
					ct.Prev = nil
				}
				return nil
			}
		}
	}
	return nil
}

// getTableName определяет имя таблицы
func (ct *CursorToken) getTableName(db *gorm.DB) (string, error) {
	var table string

	// если явно указано имя - вернуть
	if db.Statement.Table != "" {
		return db.Statement.Table, nil
	}

	// взять модель, найти метод TableName, вызвать его и вернуть имя
	v := reflect.ValueOf(db.Statement.Model)
	if v.IsValid() {
		m := v.MethodByName("TableName")
		if m.IsValid() {
			res := m.Call([]reflect.Value{})
			if len(res) > 0 && res[0].IsValid() {
				table = res[0].String()
			} else {
				return "", fmt.Errorf("invalid TableName() response")
			}
		} else {
			return "", fmt.Errorf("method TableName() not found in model")
		}
	} else {
		return "", fmt.Errorf("invalid statement model")
	}

	return table, nil
}

// SetTokens устанавливает токены пагинации Next и Prev
func (ct *CursorToken) SetTokens(data interface{}) error {

	// если тип данных не слайс - пагинация невозможна
	if reflect.TypeOf(data).Kind() != reflect.Slice {
		return fmt.Errorf("data is not a slice")
	}

	v := reflect.ValueOf(data)

	// если не можем получить reflect Value или длина данных 0 -
	// ставим пустые токены
	if v.IsValid() && v.Len() == 0 {
		ct.Prev = map[string]string{}
		ct.Next = map[string]string{}
		return nil
	}

	var (
		first interface{}
		last  interface{}
	)

	// если есть нулевой и последний элементы данных -
	// берем их интерфейсы
	if v.Index(0).IsValid() && v.Index(v.Len()-1).IsValid() {
		first = v.Index(0).Interface()
		last = v.Index(v.Len() - 1).Interface()
	} else {
		return fmt.Errorf("data indexes error")
	}

	next := map[string]string{}
	prev := map[string]string{}

	// проход по модификаторам сортировки
	// и заполнение токена next
	for _, o := range ct.orderConverter.modificators {

		// найти поле сортировки в структуре данных
		field, err := ct.getField(o.arg, last)
		if err != nil {
			return fmt.Errorf("field %s error: %w", field, err)
		}

		next[o.arg] = field
	}

	// проход по модификаторам сортировки
	// и заполнение токена prev
	if ct.Prev != nil || ct.Next != nil {
		for _, o := range ct.orderConverter.modificators {
			field, err := ct.getField(o.arg, first)
			if err != nil {
				return fmt.Errorf("field %s error: %w", field, err)
			}

			prev[o.arg] = field
		}
	}

	ct.Next = next
	ct.Prev = prev

	return nil
}
