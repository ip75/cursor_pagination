package pagination

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func TestNewOrderClauseConverter(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		type testCase struct {
			input []string
		}

		tests := []testCase{
			{
				input: []string{"asc(id)", "desc(id)"},
			},
			{
				input: []string{"asc(id)", "asc(last_name)"},
			},
		}

		for _, test := range tests {
			converter, err := NewOrderClauseConverter(test.input)
			assert.NoError(t, err)
			assert.NotEmpty(t, converter)
		}

	})

	t.Run("error", func(t *testing.T) {
		type testCase struct {
			input []string
		}

		tests := []testCase{
			{input: []string{"asc()"}},
			{input: []string{"wrongoperator(id)"}},
			{input: []string{"()"}},
			{input: []string{"(id)"}},
		}

		for _, test := range tests {
			converter, err := NewOrderClauseConverter(test.input)
			assert.Error(t, err)
			assert.Empty(t, converter)
		}
	})
}

func TestOrderClauseConverter_ToGORM(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		type testCase struct {
			input []string
		}

		tests := []testCase{
			{input: []string{"asc(id)"}},
			{input: []string{"asc(id)", "desc(id)"}},
		}

		// Создаём соединение с БД
		db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
		assert.NoError(t, err)
		rawDB := *db

		for _, tt := range tests {
			// Создаём преобразователь
			converter, err := NewOrderClauseConverter(tt.input)
			assert.NoError(t, err)
			assert.NotEmpty(t, converter)

			// Создаём условие для GORM'a
			db, err = converter.ToGORM(db)
			assert.NoError(t, err)

			var orderModificators []string
			for _, o := range tt.input {
				m := newOrderModificator()
				err := m.Parse(o)
				assert.NoError(t, err)

				orderModificators = append(orderModificators, m.arg+" "+m.operator)
			}

			for _, cls := range db.Statement.Clauses {
				c, ok := cls.Expression.(clause.OrderBy)
				if !ok {
					continue
				}

				assert.Equal(t, len(orderModificators), len(c.Columns))

				for _, column := range c.Columns {
					found := false
					for _, order := range orderModificators {
						if order == column.Column.Name {
							found = true
						}
					}

					if !found {
						assert.Fail(t, fmt.Sprintf("%s not found in result clauses", column.Column.Name))
					}
				}

			}

			// Сбрасываем параметры фильтрации
			db = &rawDB
		}
	})
}
