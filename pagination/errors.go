package pagination

import "errors"

var (
	ErrTokenIsInvalid = errors.New("token is invalid")
)
