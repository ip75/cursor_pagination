package pagination

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

const (
	// Limit по умолчанию
	DefaultLimit = 10
)

var (
	errPrevNextDataIsInvalid = errors.New("prev/next data for token is invalid")
)

// CursorOption опция
type CursorOption func(cursor *CursorToken)

// CursorToken - тип токена при пагинации с помощью курсора
type CursorToken struct {
	Token

	// Prev - первый отображенный элемент на текущей странице
	Prev map[string]string `json:"prev,omitempty"`

	// Next - последний отображенный элемент на текущей странице
	Next map[string]string `json:"next,omitempty"`

	orderConverter *OrderClauseConverter
	defaultId      string
	paging         bool
}

// WithDefaultId опция для задания поля id (монотонно-возрастающего)
func WithDefaultId(field string, dir ...string) CursorOption {
	return func(cursor *CursorToken) {
		direction := "asc"
		if len(dir) > 0 {
			if strings.ToLower(dir[0]) == "asc" || strings.ToLower(dir[0]) == "desc" {
				direction = strings.ToLower(dir[0])
			}
		}
		cursor.defaultId = fmt.Sprintf("%s(%s)", direction, field)
	}
}

// WithPaging опция включения/выключения пагинации
func WithPaging(paging bool) CursorOption {
	return func(cursor *CursorToken) {
		cursor.paging = paging
	}
}

// NewCursorToken конструктор для CursorToken
func NewCursorToken(opts ...CursorOption) *CursorToken {
	defaultId := "asc(id)"
	cursor := &CursorToken{
		defaultId: defaultId,
		paging:    true,
	}

	for _, opt := range opts {
		opt(cursor)
	}

	return cursor
}

// GeneratePrevToken генерирует prev токен; prevData - хранит последний для отображённый элемент на странице с ключом для этого элемента
func (ct *CursorToken) GeneratePrevToken(prevData map[string]string) (string, error) {
	if len(prevData) == 0 {
		return "", nil
	}

	if err := ct.validatePrevNext(prevData); err != nil {
		return "", err
	}

	t := *ct
	t.Prev = prevData
	t.Next = nil

	return t.generateBase64Token()
}

// GenerateNextToken генерирует next токен; nextData - хранит последний для отображённый элемент на странице с ключом для этого элемента
func (ct *CursorToken) GenerateNextToken(nextData map[string]string) (string, error) {
	if len(nextData) == 0 {
		return "", nil
	}

	if err := ct.validatePrevNext(nextData); err != nil {
		return "", err
	}

	t := *ct
	t.Next = nextData
	t.Prev = nil

	return t.generateBase64Token()
}

// ParseToken разбирает токен в формате base64
func (ct *CursorToken) ParseToken(encodedToken string) error {
	// Проверяем валидность токена
	if len(encodedToken) == 0 {
		return fmt.Errorf("cursor token: %w", ErrTokenIsInvalid)
	}

	token, err := base64.RawURLEncoding.DecodeString(encodedToken)
	if err != nil {
		return err
	}

	// Разбираем токен в структуру
	if err := json.Unmarshal(token, ct); err != nil {
		return err
	}

	return nil
}

// ParseQuery заполняет структуру токена из query-параметров
func (ct *CursorToken) ParseQuery(query string) error {
	// Разбираем строку GET - запроса
	params, err := url.ParseQuery(query)
	if err != nil {
		return err
	}

	// Получение курсора из строки запроса
	cursor := params.Get(cursorParam)
	if len(cursor) > 0 {
		if err := ct.ParseToken(cursor); err != nil {
			return err
		}
		return nil
	}

	// Курсор не был передан, разбираем строку с запросом
	if err := ct.Token.ParseQuery(query); err != nil {
		return err
	}

	return nil
}

// validatePrevNext валидирует prev/next данные токена
func (ct *CursorToken) validatePrevNext(data map[string]string) error {
	for k, v := range data {
		if k == "" {
			return fmt.Errorf("%w: key is empty", errPrevNextDataIsInvalid)
		}

		if v == "" {
			return fmt.Errorf("%w: value for key (%s) is empty", errPrevNextDataIsInvalid, k)
		}
	}

	return nil
}

// generateBase64Token генерирует токен в строку в формате base64
func (ct *CursorToken) generateBase64Token() (string, error) {
	var buf bytes.Buffer
	encoder := base64.NewEncoder(base64.RawURLEncoding, &buf)
	err := json.NewEncoder(encoder).Encode(ct)
	if err != nil {
		return "", err
	}

	if err := encoder.Close(); err != nil {
		return "", nil
	}

	return buf.String(), nil
}

// getFilters формирует список фильтрации
func (ct *CursorToken) getFilters(table string, allowedFilters map[string]string, fieldMapping map[string]string) ([]string, error) {
	var filters []string
	conds := map[string][]string{}
	condsType := "string"

	for column, conditions := range ct.Filter {
		if column == cursorParam {
			continue
		}

		// если поле фильтрации отсутствует в разрешенных (не найдено)
		arrayColumn := false
		if strings.HasSuffix(column, "[]") {
			arrayColumn = true
			column = strings.TrimSuffix(column, "[]")
			if _, ok := conds[fmt.Sprintf("%s.%s", table, column)]; !ok {
				conds[fmt.Sprintf("%s.%s", table, column)] = []string{}
			}
		}
		filterType, ok := allowedFilters[column]
		if !ok {
			return nil, fmt.Errorf("filter not allowed: %s", column)
		}

		col := fmt.Sprintf("%s.%s", table, column)
		mappedTable, ok := fieldMapping[column]
		if ok {
			col = mappedTable
		}

		for _, cond := range conditions {
			cond = strings.ReplaceAll(cond, "'", "\\'")

			if arrayColumn {
				switch filterType {
				case "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16", "uint32", "uint64", "bool":
					condsType = "int"
					conds[col] = append(conds[col], cond)
				default:
					condsType = "string"
					conds[col] = append(conds[col], fmt.Sprintf("'%s'", cond))
				}
				continue
			}

			m := newFilterModificator()
			if err := m.Parse(cond); err != nil {
				if errors.Is(err, errModificatorIsEmpty) {
					switch filterType {
					case "Time":
						filters = append(filters, fmt.Sprintf("extract(epoch from %s)::integer = %s", col, cond))
					default:
						filters = append(filters, fmt.Sprintf("%s = '%s'", col, cond))
					}
				}
				continue
			}

			if err := m.Validate(); err != nil {
				continue
			}

			filterCol := col

			// в зависимости от типа поля - заключаем в кавычки
			val := fmt.Sprintf("%s", m.arg)
			switch filterType {
			case "string":
				val = fmt.Sprintf("'%s'", m.arg)
			case "Time":
				filterCol = fmt.Sprintf("extract(epoch from %s)::integer", col)
			}

			// Составляем условие исходя из оператора поиска
			switch m.operator {
			case filterGt:
				filters = append(filters, fmt.Sprintf("%s > %s", filterCol, val))
			case filterGe:
				filters = append(filters, fmt.Sprintf("%s >= %s", filterCol, val))
			case filterLt:
				filters = append(filters, fmt.Sprintf("%s < %s", filterCol, val))
			case filterLe:
				filters = append(filters, fmt.Sprintf("%s <= %s", filterCol, val))
			case filterLike:
				filters = append(filters, fmt.Sprintf("%s::text ILIKE '%s'", filterCol, "%"+m.arg+"%"))
			}
		}

	}
	for column, v := range conds {
		switch condsType {
		case "int":
			filters = append(filters, fmt.Sprintf("%s IN (%s)", column, strings.Join(v, ",")))
		default:
			filters = append(filters, fmt.Sprintf("%s::text IN (%s)", column, strings.Join(v, ",")))
		}
	}

	return filters, nil
}

// getFieldMapping формирует маппинги полей из SQL (a.id AS id, ...)
func (ct *CursorToken) getFieldMapping(sql string) map[string]string {
	fieldMapping := map[string]string{}

	// очистить SQL от лишних символов
	purified := strings.ReplaceAll(sql, "\n", "")
	purified = strings.ReplaceAll(purified, "\r", "")
	purified = strings.ReplaceAll(purified, "\t", "")
	richSelect := ""
	re := regexp.MustCompile("(?i)(\\s+)AS(\\s+)")
	for _, oneSelect := range strings.Split(purified, ",") {
		if strings.ContainsAny(oneSelect, "(") {
			richSelect += oneSelect + ","
			continue
		}
		if len(richSelect) != 0 && !strings.ContainsAny(oneSelect, ")") {
			richSelect += oneSelect + ","
			continue
		}
		if strings.ContainsAny(oneSelect, ")") {
			richSelect += oneSelect
		}
		if richSelect != "" {
			oneSelect = richSelect
			richSelect = ""
		}
		as := re.Split(oneSelect, -1)
		if len(as) == 2 {
			fieldMapping[strings.Trim(as[1], " ")] = strings.Trim(as[0], " ")
		}
	}
	return fieldMapping
}

// getSelects конструирует строку SELECT и возвращает маппинг полей
func (ct *CursorToken) getSelects(db *gorm.DB) (string, map[string]string) {
	fieldMapping := map[string]string{}

	selects := ""
	if db.Statement.Selects != nil && len(db.Statement.Selects) > 0 {
		// если в GORM есть Selects (без clauses)
		for _, sel := range db.Statement.Selects {
			fieldMapping = ct.getFieldMapping(sel)
		}

		selects = strings.Join(db.Statement.Selects, ",")
	} else {
		// если в GORM определены Selects (с clauses)
		// проход по Clauses
		for cname, c := range db.Statement.Clauses {
			if cname == "SELECT" {
				e, ok := c.Expression.(clause.Expr)
				if ok {

					// найти все GORM-clauses (поддерживаются Table, Column)
					args := []interface{}{}
					for _, v := range e.Vars {
						if ct, ok := v.(clause.Table); ok {
							args = append(args, ct.Name)
						}
						if ct, ok := v.(clause.Column); ok {
							args = append(args, ct.Name)
						}
					}

					// заменить ? на значения из clauses
					replaced := strings.ReplaceAll(e.SQL, "?", "%v")
					selects = fmt.Sprintf(replaced, args...)
					fieldMapping = ct.getFieldMapping(selects)
					break
				}
			}
		}
	}

	// если нет полей для SELECT - выбираем все
	if selects == "" {
		selects = "*"
	}

	return selects, fieldMapping
}

// isMixedOrder проверяет, смешанная ли пагинация (asc, desc), а если нет - возвращает тип пагинации
func (ct *CursorToken) isMixedOrder() (string, bool) {
	asc := 0
	desc := 0
	for _, order := range ct.Order {
		if strings.HasPrefix(order, "asc") {
			asc++
		}
		if strings.HasPrefix(order, "desc") {
			desc++
		}
	}
	if asc > 0 && desc > 0 {
		return "", true
	}
	if asc > 0 {
		return "ASC", false
	}
	return "DESC", false
}

// getNext конструирует запрос для следующей страницы пагинации
func (ct *CursorToken) getNext(db *gorm.DB, table string, fieldMapping map[string]string, filters []string) *gorm.DB {

	ct.addFilters(db, filters)

	// проход по полям из токена next
	for field, v := range ct.Next {

		// проход по полям сортировки
		for _, order := range ct.Order {

			// определяем порядок сортировки
			orderType := "ASC"
			orderOpr := ">"
			neg := ""
			if fmt.Sprintf("desc(%s)", field) == order {
				orderType = "DESC"
				if _, mixed := ct.isMixedOrder(); mixed {
					orderOpr = ">"
					neg = "-"
				} else {
					orderOpr = "<"
				}
			} else if fmt.Sprintf("asc(%s)", field) == order {
				orderType = "ASC"
				orderOpr = ">"
			} else {
				continue
			}

			// fieldMapping содержит маппинг имен полей из запросов вида
			// SELECT table1.id AS t1_id, table2.id AS t2_id
			mappedField, ok := fieldMapping[field]
			if ok {
				db = db.Where(fmt.Sprintf("%s%s %s %s", neg, mappedField, orderOpr, neg+v))
				db = db.Order(fmt.Sprintf("%s %s", mappedField, orderType))
			} else {
				db = db.Where(fmt.Sprintf("%s%s.%s %s %s", neg, table, field, orderOpr, neg+v))
				db = db.Order(fmt.Sprintf("%s.%s %s", table, field, orderType))
			}
		}
	}

	return db
}

// getNext конструирует запрос для предыдущей страницы пагинации
func (ct *CursorToken) getPrev(db *gorm.DB, table string, fieldMapping map[string]string, selects string, filters []string, data interface{}) *gorm.DB {
	var (
		pageFields []string
		pageVals   []string
		orderInner []string
		orderOuter []string
	)

	// проход по полям из токена prev
	for field, val := range ct.Prev {
		if _, err := strconv.Atoi(val); err != nil {
			val = "'" + val + "'"
		}

		// проход по модификаторам сортировки
		for _, mod := range ct.orderConverter.modificators {

			// добавление полей сортировки во внутренний и внешний ORDER BY
			if mod.arg == field {
				if mod.operator == "asc" {
					pageVals = append(pageVals, val)
					mappedField, ok := fieldMapping[field]
					if ok {
						pageFields = append(pageFields, mappedField)
						orderInner = append(orderInner, fmt.Sprintf("%s DESC", mappedField))
					} else {
						pageFields = append(pageFields, table+"."+field)
						orderInner = append(orderInner, fmt.Sprintf("%s.%s DESC", table, field))
					}
					orderOuter = append(orderOuter, fmt.Sprintf("page.%s ASC", field))
				} else {
					neg := ""
					if _, mixed := ct.isMixedOrder(); mixed {
						neg = "-"
					}
					pageVals = append(pageVals, neg+val)
					mappedField, ok := fieldMapping[field]
					if ok {
						pageFields = append(pageFields, mappedField)
						orderInner = append(orderInner, fmt.Sprintf("%s ASC", mappedField))
					} else {
						pageFields = append(pageFields, neg+table+"."+field)
						orderInner = append(orderInner, fmt.Sprintf("%s.%s ASC", table, field))
					}
					orderOuter = append(orderOuter, fmt.Sprintf("page.%s DESC", field))
				}
			}
		}
	}

	// добавить к фильтрам условия пагинации
	opr := "<"
	oType, mixed := ct.isMixedOrder()
	if !mixed && oType == "DESC" {
		opr = ">"
	}
	filters = append(filters,
		fmt.Sprintf("(%s) %s (%s)",
			strings.Join(pageFields, ","),
			opr,
			strings.Join(pageVals, ",")),
	)

	// добавить к фильтрам WHERE clauses из GORM
	for cname, c := range db.Statement.Clauses {
		if cname == "WHERE" {
			e, ok := c.Expression.(clause.Where)
			if ok {
				for _, exp := range e.Exprs {
					if whereExp, ok := exp.(clause.Expr); ok {
						replaced := strings.ReplaceAll(whereExp.SQL, "?", "%v")
						var vars []interface{}
						for _, v := range whereExp.Vars {
							switch ct := v.(type) {
							case clause.Table:
								vars = append(vars, v.(clause.Table).Name)
							case clause.Column:
								vars = append(vars, v.(clause.Column).Name)
							case string:
								vars = append(vars, fmt.Sprintf("'%s'", v))
							case int:
								vars = append(vars, v)
							case []string:
								ins := []string{}
								for _, s := range ct {
									ins = append(ins, fmt.Sprintf("'%s'", s))
								}
								vars = append(vars, fmt.Sprintf("(%s)", strings.Join(ins, ",")))
							case []int:
								ins := []string{}
								for _, s := range ct {
									ins = append(ins, fmt.Sprintf("%d", s))
								}
								vars = append(vars, fmt.Sprintf("(%s)", strings.Join(ins, ",")))
							}
						}
						filters = append(filters, fmt.Sprintf(replaced, vars...))
						break
					}
				}
			}
		}
	}

	// формируем полную строку фильтрации
	filterString := strings.Join(filters, " and ")

	// формируем полные строки сортировки
	innerOrderString := strings.Join(orderInner, ",")
	outerOrderString := strings.Join(orderOuter, ",")

	// формируем полную строку JOIN
	joins, selects := ct.getJoins(db, data, selects)
	joinsString := strings.Join(joins, " ")

	groupByString := ct.getGroupBy(db)
	if groupByString != "" {
		groupByString = "GROUP BY " + groupByString
	}

	// финальный запрос в RAW
	s := fmt.Sprintf(`
			WITH page AS (SELECT %s
			              FROM %s %s
			              WHERE %s
						  %s
			              ORDER BY %s
			              LIMIT ?)
			SELECT * FROM page ORDER BY %s`, selects, table, joinsString, filterString, groupByString, innerOrderString, outerOrderString)
	db = db.Raw(s, ct.Limit+1)

	return db
}

func (ct *CursorToken) getGroupBy(db *gorm.DB) string {
	var groups []string

	for _, c := range db.Statement.Clauses {
		if c.Name == "GROUP BY" {
			e, ok := c.Expression.(clause.GroupBy)
			if !ok {
				continue
			}
			for _, col := range e.Columns {
				groups = append(groups, col.Name)
			}
		}
	}

	return strings.Join(groups, ",")
}

func (ct *CursorToken) getJoins(db *gorm.DB, data interface{}, selects string) ([]string, string) {
	joins := []string{}
	for _, j := range db.Statement.Joins {
		args := []interface{}{}

		if j.Conds == nil || len(j.Conds) == 0 {
			parts := strings.Split(j.Name, " ")
			if len(parts) == 1 {
				// это join с таблицей
				join, sel := ct.getJoinFromStruct(data, j.Name)
				joins = []string{join}
				return joins, sel
			}
		}

		for _, a := range j.Conds {
			switch a.(type) {
			case string:
				args = append(args, a)
			case clause.Table:
				args = append(args, a.(clause.Table).Name)
			case clause.Column:
				args = append(args, a.(clause.Column).Name)
			}
		}
		replaced := strings.ReplaceAll(j.Name, "?", "%v")
		joins = append(joins, fmt.Sprintf(replaced, args...))
	}
	return joins, selects
}

// addFilters добавляет фильтры в запрос Gorm
func (ct *CursorToken) addFilters(db *gorm.DB, filters []string) *gorm.DB {
	for _, f := range filters {
		db.Where(f)
	}

	return db
}

// getFirst конструирует запрос для первой страницы пагинации
func (ct *CursorToken) getFirst(db *gorm.DB, table string, fieldMapping map[string]string, filters []string) *gorm.DB {

	ct.addFilters(db, filters)

	// fieldMapping содержит маппинг имен полей из запросов вида
	// SELECT table1.id AS t1_id, table2.id AS t2_id
	for _, mod := range ct.orderConverter.modificators {
		if mod.operator == "asc" {
			mappedField, ok := fieldMapping[mod.arg]
			if ok {
				db = db.Order(fmt.Sprintf("%s ASC", mappedField))
			} else {
				db = db.Order(fmt.Sprintf("%s.%s ASC", table, mod.arg))
			}
		} else {
			mappedField, ok := fieldMapping[mod.arg]
			if ok {
				db = db.Order(fmt.Sprintf("%s DESC", mappedField))
			} else {
				db = db.Order(fmt.Sprintf("%s.%s DESC", table, mod.arg))
			}
		}
	}

	return db
}

func (ct *CursorToken) hasId() bool {
	re := regexp.MustCompile("^.*\\(id\\)$")
	for _, o := range ct.Order {
		if re.MatchString(o) {
			return true
		}
	}
	return false
}

// buildQuery конвертирует запросы в структуры GORM
func (ct *CursorToken) buildQuery(db *gorm.DB, data interface{}) (*gorm.DB, error) {

	// только при включенной пагинации
	// поставим лимит в n+1 для определения предыдущей и следующей страницы
	if ct.paging {
		if ct.Limit <= 0 {
			ct.Limit = DefaultLimit
		}
		db = db.Limit(int(ct.Limit + 1))
	} else {
		db = db.Limit(int(ct.Limit))
	}

	// определить имя таблицы (нужно для join-запросов)
	table, err := ct.getTableName(db)
	if err != nil {
		return nil, fmt.Errorf("can't determine table name: %w", err)
	}

	// определить допустимые фильтры по полям результирующей структуры
	allowedFilters, err := ct.getAllowedFilters(data)
	if err != nil {
		return nil, fmt.Errorf("can't get allowed filters: %w", err)
	}

	// только при включенной пагинации
	// добавить в сортировку id при его отсутствии
	if ct.paging {
		if !ct.hasId() {
			ct.Order = append(ct.Order, ct.defaultId)
		}
	}

	// выставить сортировку по параметру order
	order, err := NewOrderClauseConverter(ct.Order)
	if err != nil {
		return nil, fmt.Errorf("can't create order converter: %w", err)
	}

	ct.orderConverter = order

	// получить поля для SELECT (нужно для join-запросов)
	selects, fieldMapping := ct.getSelects(db)

	// получить фильтры с учетом допустимых
	filters, err := ct.getFilters(table, allowedFilters, fieldMapping)
	if err != nil {
		return db, fmt.Errorf("can't get filters: %w", err)
	}

	// если нет токенов пагинации - получить первую страницу
	if ct.Next == nil && ct.Prev == nil {
		db = ct.getFirst(db, table, fieldMapping, filters)
	}

	// получить следующую страницу
	if ct.Next != nil {
		db = ct.getNext(db, table, fieldMapping, filters)
	}

	// получить предыдущую страницу
	if ct.Prev != nil {
		db = ct.getPrev(db, table, fieldMapping, selects, filters, data)
	}

	return db, nil
}

// Scan является оберткой над GORM Scan, но поддерживает курсорную пагинацию
func (ct *CursorToken) Scan(db *gorm.DB, data interface{}) (*gorm.DB, error) {

	// преобразуем запрос в структуры GORM
	db, err := ct.buildQuery(db, data)
	if err != nil {
		return db, fmt.Errorf("can't buildQuery: %w", err)
	}

	// запрос данных
	db = db.Scan(data)

	if db.Error != nil {
		return db, fmt.Errorf("scan error: %w", db.Error)
	}

	err = ct.setData(db.Statement.ReflectValue, data)
	if err != nil {
		return db, fmt.Errorf("can't set data: %w", err)
	}

	return db, nil
}
