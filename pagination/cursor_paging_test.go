package pagination

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"testing"
	"time"
)

const (
	TestDataLen  = 50
	TestPageSize = 10
)

type EnumStatus string
type DateTime int64

type BaseModel struct {
	ID        int64 `gorm:"primaryKey;auto_increment;unique"`
	CreatedAt int64 `gorm:"<-:create;autoCreateTime"`
}

type cursorTableOne struct {
	BaseModel
	UUID       string `gorm:"unique"`
	Name       string
	Number     int
	Two        cursorTableTwo `gorm:"foreignKey:UUID;references:UUID"`
	Status     sql.NullString `gorm:"type:enum_status;default:'hold'"`
	TotalPrice sql.NullInt64
	Ts         DateTime
	UpdatedAt  time.Time
	ExecutedAt time.Time
	LastName   string
	FirstName  string
	SecondName string
}

func (cursorTableOne) TableName() string {
	return "cursor_table_one"
}

type cursorTableTwo struct {
	BaseModel
	UUID   string
	Amount int64
	Status EnumStatus `gorm:"type:enum_status;default:'hold'"`
}

func (cursorTableTwo) TableName() string {
	return "cursor_table_two"
}

type joinedTable struct {
	BaseModel
	OneID    int64 `json:"one_id"`
	TwoID    int64 `json:"two_id"`
	Name     string
	OneUUID  string
	TwoUUID  string
	Number   int `json:"number"`
	Amount   int64
	Status   string `json:"status"`
	FullName string `json:"full_name"`
}

type CursorSuite struct {
	suite.Suite
	db *gorm.DB
}

func TestCursorSuite(t *testing.T) {
	suite.Run(t, &CursorSuite{})
}

func (s *CursorSuite) SetupSuite() {
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	conn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		"localhost",
		5432,
		"postgres",
		"12345678",
		"lib",
		"",
	)
	driver := postgres.Open(conn)

	db, err := gorm.Open(driver, &gorm.Config{})
	if err != nil {
		return
	}

	s.db = db

	s.createTables()
	s.generateData()
}

func (s *CursorSuite) generateData() {
	ts := time.Now()
	for i := 1; i <= TestDataLen; i++ {
		oneUUID := uuid.NewString()
		s.db.Model(&cursorTableOne{}).Create(&cursorTableOne{
			BaseModel: BaseModel{
				ID:        int64(i),
				CreatedAt: ts.Unix(),
			},
			UUID:   oneUUID,
			Name:   fmt.Sprintf("строка номер %d", i),
			Number: 400000,
			Ts:     DateTime(ts.Unix()),
			TotalPrice: sql.NullInt64{
				Int64: 200,
				Valid: true,
			},
			UpdatedAt: time.Now(),
			LastName:  "Иванов",
			FirstName: "Иван",
		})
		ts = ts.Add(time.Second)

		s.db.Model(&cursorTableTwo{}).Create(&cursorTableTwo{
			BaseModel: BaseModel{
				ID:        int64(i),
				CreatedAt: ts.Unix(),
			},
			UUID:   oneUUID,
			Amount: 300000,
			Status: "hold",
		})
	}
}

func (s *CursorSuite) createTables() {
	s.db.Exec("DO $$ " +
		"BEGIN " +
		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'enum_status') THEN " +
		"CREATE TYPE enum_status AS ENUM ('hold','stop');" +
		"END IF;" +
		"END$$;")
	err := s.db.AutoMigrate(
		cursorTableOne{},
		cursorTableTwo{},
	)
	if err != nil {
		panic(err)
	}
}

func (s *CursorSuite) dropTables() {
	s.db.Exec("DROP TABLE ?", clause.Table{Name: "cursor_table_two"})
	s.db.Exec("DROP TABLE ?", clause.Table{Name: "cursor_table_one"})
	s.db.Exec("DROP TYPE enum_status")
}

func (s *CursorSuite) TearDownSuite() {
	s.dropTables()

	conn, err := s.db.DB()
	if err != nil {
		panic(err)
	}
	err = conn.Close()
	if err != nil {
		panic(err)
	}
}

func (s *CursorSuite) requestJoins(cToken *CursorToken) []joinedTable {
	db := s.db.
		Select(`?.*, ?.*, cursor_table_two.status AS status,
			cursor_table_one.created_at AS created_at,
			cursor_table_one.number AS number,
			cursor_table_two.amount AS amount,
			cursor_table_one.uuid AS one_uuid,
			cursor_table_two.uuid AS two_uuid,
			cursor_table_one.id as one_id, cursor_table_two.id as two_id`,
			clause.Table{Name: "cursor_table_one"}, clause.Table{Name: "cursor_table_two"}).
		Joins("left join ? on ?.? = ?.?", clause.Table{Name: "cursor_table_two"},
			clause.Table{Name: "cursor_table_two"}, clause.Column{Name: "id"},
			clause.Table{Name: "cursor_table_one"}, clause.Column{Name: "id"},
		).
		Where("?.status IN ?", clause.Table{Name: "cursor_table_two"}, []string{"hold", "stop"}).
		Where("cursor_table_two.status IN ?", []string{"hold", "stop"}).
		Where("?.status = ?", clause.Table{Name: "cursor_table_one"}, "hold").
		Where("?.status = ?", clause.Table{Name: "cursor_table_two"}, "hold").
		Where("?.amount IN ?", clause.Table{Name: "cursor_table_two"}, []int{0, 300000}).
		Where("?.number IN ?", clause.Table{Name: "cursor_table_one"}, []int{0, 400000})

	var joined []joinedTable

	resDB, err := cToken.Scan(db.Model(cursorTableOne{}), &joined)
	s.NoError(err)
	s.NoError(resDB.Error)

	result := make([]joinedTable, 0, len(joined))
	for _, j := range joined {
		result = append(result, joinedTable{
			OneID:   j.OneID,
			TwoID:   j.TwoID,
			OneUUID: j.OneUUID,
			TwoUUID: j.TwoUUID,
			Name:    j.Name,
			Number:  j.Number,
			Amount:  j.Amount,
		})
	}

	return result
}

func (s *CursorSuite) requestJoinsGroupBy(cToken *CursorToken) []joinedTable {
	db := s.db.
		Select(`?.*,
			cursor_table_one.id AS one_id,
			ARRAY_AGG(cursor_table_two.uuid) AS two_uuid`,
			clause.Table{Name: "cursor_table_one"}).
		Joins("left join ? on ?.? = ?.?", clause.Table{Name: "cursor_table_two"},
			clause.Table{Name: "cursor_table_two"}, clause.Column{Name: "id"},
			clause.Table{Name: "cursor_table_one"}, clause.Column{Name: "id"},
		).
		Group("cursor_table_one.id, cursor_table_two.uuid")

	var joined []joinedTable

	resDB, err := cToken.Scan(db.Model(cursorTableOne{}), &joined)
	s.NoError(err)
	s.NoError(resDB.Error)

	result := make([]joinedTable, 0, len(joined))
	for _, j := range joined {
		result = append(result, joinedTable{
			OneID:   j.OneID,
			TwoID:   j.TwoID,
			OneUUID: j.OneUUID,
			TwoUUID: j.TwoUUID,
			Name:    j.Name,
			Number:  j.Number,
			Amount:  j.Amount,
		})
	}

	return result
}

func (s *CursorSuite) requestPreloadJoins(cToken *CursorToken) []cursorTableOne {
	db := s.db.Model(&cursorTableOne{}).
		Joins("Two")

	var joined []cursorTableOne

	resDB, err := cToken.Scan(db.Model(cursorTableOne{}), &joined)
	s.NoError(err)
	s.NoError(resDB.Error)

	result := make([]joinedTable, 0, len(joined))
	for _, j := range joined {
		result = append(result, joinedTable{
			Name: j.Name,
		})
	}

	return joined
}

func (s *CursorSuite) requestSimple(cToken *CursorToken) []cursorTableOne {
	db := s.db.Model(&cursorTableOne{})

	var tableOne []cursorTableOne

	resDB, err := cToken.Scan(db.Model(cursorTableOne{}), &tableOne)
	s.NoError(err)
	s.NoError(resDB.Error)

	return tableOne
}

func (s *CursorSuite) requestSimpleConcat(cToken *CursorToken) []joinedTable {
	db := s.db.Model(&cursorTableOne{}).
		Select("?.*, ?.*, CONCAT(last_name, ' ', first_name, ' ', second_name) AS full_name",
			clause.Table{Name: "cursor_table_one"}, clause.Table{Name: "cursor_table_two"}).
		Joins("left join ? on ?.? = ?.?", clause.Table{Name: "cursor_table_two"},
			clause.Table{Name: "cursor_table_two"}, clause.Column{Name: "id"},
			clause.Table{Name: "cursor_table_one"}, clause.Column{Name: "id"},
		)

	var tableOne []joinedTable

	resDB, err := cToken.Scan(db.Model(cursorTableOne{}), &tableOne)
	s.NoError(err)
	s.NoError(resDB.Error)

	return tableOne
}

func (s *CursorSuite) requestSimpleClauses(cToken *CursorToken) []cursorTableOne {
	db := s.db.Model(&cursorTableOne{}).
		Where("? > ?", clause.Column{Name: "number"}, 0)

	var tableOne []cursorTableOne

	resDB, err := cToken.Scan(db, &tableOne)
	s.NoError(err)
	s.NoError(resDB.Error)

	return tableOne
}

func (s *CursorSuite) queryJoins(params string) (joins []joinedTable, next, prev string) {
	cToken := NewCursorToken(WithDefaultId("one_id", "asc"))
	err := cToken.ParseQuery(params)
	s.NoError(err)

	joins = s.requestJoins(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) queryJoinsGroupBy(params string) (joins []joinedTable, next, prev string) {
	cToken := NewCursorToken(WithDefaultId("one_id", "asc"))
	err := cToken.ParseQuery(params)
	s.NoError(err)

	joins = s.requestJoinsGroupBy(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) queryDisabledPagingJoins(params string) (joins []joinedTable, next, prev string) {
	cToken := NewCursorToken(
		WithDefaultId("one_id", "asc"),
		WithPaging(false),
	)
	err := cToken.ParseQuery(params)
	s.NoError(err)

	joins = s.requestJoins(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) queryPreloadJoins(params string) (joins []cursorTableOne, next, prev string) {
	cToken := NewCursorToken(WithDefaultId("id", "asc"))
	err := cToken.ParseQuery(params)
	s.NoError(err)

	joins = s.requestPreloadJoins(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) querySimple(params string) (tableOne []cursorTableOne, next, prev string) {
	cToken := NewCursorToken()
	err := cToken.ParseQuery(params)
	s.NoError(err)

	tableOne = s.requestSimple(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) querySimpleConcat(params string) (tableOne []joinedTable, next, prev string) {
	cToken := NewCursorToken(WithPaging(false))
	err := cToken.ParseQuery(params)
	s.NoError(err)

	tableOne = s.requestSimpleConcat(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) querySimpleConcatPaging(params string) (tableOne []joinedTable, next, prev string) {
	cToken := NewCursorToken()
	err := cToken.ParseQuery(params)
	s.NoError(err)

	tableOne = s.requestSimpleConcat(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) querySimpleClauses(params string) (tableOne []cursorTableOne, next, prev string) {
	cToken := NewCursorToken()
	err := cToken.ParseQuery(params)
	s.NoError(err)

	tableOne = s.requestSimpleClauses(cToken)

	next, err = cToken.GenerateNextToken(cToken.Next)
	s.NoError(err)
	prev, err = cToken.GeneratePrevToken(cToken.Prev)
	s.NoError(err)

	return
}

func (s *CursorSuite) TestCursor_DisabledPagingJoinsAsc() {
	results, next, prev := s.queryDisabledPagingJoins(fmt.Sprintf("status=hold&amount=gt(0)"))

	s.Equal(len(results), TestDataLen)
	s.Empty(prev)
	s.Empty(next)

	results, next, prev = s.queryDisabledPagingJoins(fmt.Sprintf("limit=5&status=hold&amount=gt(0)"))

	s.Equal(len(results), 5)
	s.Empty(prev)
	s.Empty(next)
}

func (s *CursorSuite) TestCursor_ConcatAsc() {
	results, next, prev := s.querySimpleConcatPaging(fmt.Sprintf("status=hold&full_name=like(иванов иван)"))

	s.Equal(len(results), TestPageSize)
	s.Empty(prev)
	s.NotEmpty(next)

	results, next, prev = s.querySimpleConcatPaging(fmt.Sprintf("limit=5"))

	s.Equal(len(results), 5)
	s.Empty(prev)
	s.NotEmpty(next)
}

func (s *CursorSuite) TestCursor_DisabledPagingJoinsConcatAsc() {
	results, next, prev := s.querySimpleConcat(fmt.Sprintf("status=hold&full_name=like(иван)"))

	s.Equal(len(results), TestDataLen)
	s.Empty(prev)
	s.Empty(next)

	results, next, prev = s.querySimpleConcat(fmt.Sprintf("limit=5"))

	s.Equal(len(results), 5)
	s.Empty(prev)
	s.Empty(next)
}

func (s *CursorSuite) TestCursor_Filters() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryJoins(fmt.Sprintf("limit=%d&name=like(Строка Номер)", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryJoins("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryJoins("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_Filters2() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryJoins(fmt.Sprintf("limit=%d&name=like(Строка)", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryJoins("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryJoins("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_Filters3() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryJoins(fmt.Sprintf("limit=%d&name=like(номер)", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryJoins("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryJoins("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_Filters4() {
	results, next, prev := s.querySimple(fmt.Sprintf("id[]=1&id[]=3&id[]=8"))

	s.Equal(len(results), 3)
	s.Empty(prev)
	s.Empty(next)
}

func (s *CursorSuite) TestCursor_Filters5() {
	results, next, prev := s.querySimple(fmt.Sprintf("status[]=hold&status[]=stop"))

	s.Equal(len(results), TestPageSize)
	s.Empty(prev)
	s.NotEmpty(next)
}

func (s *CursorSuite) TestCursor_JoinsGroupBy() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryJoinsGroupBy(fmt.Sprintf("limit=%d", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryJoinsGroupBy("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryJoinsGroupBy("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_JoinsAsc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryJoins(fmt.Sprintf("limit=%d&status=hold&amount=gt(0)", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryJoins("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryJoins("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.Equal(results[0].OneID, int64(step*limit+1))
			s.Equal(results[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(results[0].OneUUID)
			s.NotEmpty(results[0].TwoUUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_JoinsDesc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	result, next, prev := s.queryJoins(fmt.Sprintf("limit=%d&status=like(hol)&name=like(строка номер)", limit))

	s.Equal(len(result), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		result, next, prev = s.queryJoins("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].OneID, int64(step*limit+1))
			s.Equal(result[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		result, next, prev = s.queryJoins("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].OneID, int64(step*limit+1))
			s.Equal(result[limit-1].OneID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_Limit_1() {
	result, next, prev := s.querySimple(fmt.Sprintf("limit=1"))

	s.Equal(len(result), 1)
	s.Empty(prev)
	s.NotEmpty(next)
	s.Equal(result[0].ID, int64(1))

	result, next, prev = s.querySimple(fmt.Sprintf("cursor=%s", next))

	s.Equal(len(result), 1)
	s.NotEmpty(prev)
	s.NotEmpty(next)
	s.Equal(result[0].ID, int64(2))

	result, next, prev = s.querySimple(fmt.Sprintf("cursor=%s", prev))

	s.Equal(len(result), 1)
	s.Empty(prev)
	s.NotEmpty(next)
	s.Equal(result[0].ID, int64(1))
}

func (s *CursorSuite) TestCursor_SimpleAsc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	result, next, prev := s.querySimple(fmt.Sprintf("limit=%d&order=asc(id)&number=gt(200000)&total_price=gt(100)&updated_at=gt(0)", limit))

	s.Equal(len(result), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		result, next, prev = s.querySimple("cursor=" + next)
		step++

		s.NotNil(result)

		if next != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(step*limit+1))
			s.Equal(result[limit-1].ID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		result, next, prev = s.querySimple("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(step*limit+1))
			s.Equal(result[limit-1].ID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_SimpleDesc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	result, next, prev := s.querySimple(fmt.Sprintf("limit=%d&order=desc(id)&number=gt(0)", limit))

	s.Equal(len(result), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		result, next, prev = s.querySimple("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(TestDataLen-step*limit))
			s.Equal(result[limit-1].ID, int64(TestDataLen-step*limit-limit+1))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		result, next, prev = s.querySimple("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(TestDataLen-step*limit))
			s.Equal(result[limit-1].ID, int64(TestDataLen-step*limit-limit+1))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_SimpleClausesAsc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	result, next, prev := s.querySimpleClauses(fmt.Sprintf("limit=%d&order=asc(id)", limit))

	s.Equal(len(result), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		result, next, prev = s.querySimpleClauses("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(step*limit+1))
			s.Equal(result[limit-1].ID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		result, next, prev = s.querySimpleClauses("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(step*limit+1))
			s.Equal(result[limit-1].ID, int64(step*limit+limit))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_SimpleClausesDesc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	result, next, prev := s.querySimpleClauses(fmt.Sprintf("limit=%d&order=desc(id)", limit))

	s.Equal(len(result), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		result, next, prev = s.querySimpleClauses("cursor=" + next)
		step++

		if next != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(TestDataLen-step*limit))
			s.Equal(result[limit-1].ID, int64(TestDataLen-step*limit-limit+1))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		result, next, prev = s.querySimpleClauses("cursor=" + prev)
		step--

		if prev != "" {
			s.Equal(len(result), limit)
			s.Equal(result[0].ID, int64(TestDataLen-step*limit))
			s.Equal(result[limit-1].ID, int64(TestDataLen-step*limit-limit+1))
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(result), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_PreloadJoinsAsc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryPreloadJoins(fmt.Sprintf("limit=%d&order=asc(created_at)&status[]=hold&status[]=stop", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryPreloadJoins("cursor=" + next)
		s.NotNil(results)
		if results == nil {
			break
		}
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.NotEmpty(results[0].UUID)
			s.NotEmpty(results[0].Two.UUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryPreloadJoins("cursor=" + prev)
		s.NotNil(results)
		if results == nil {
			break
		}
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.NotEmpty(results[0].UUID)
			s.NotEmpty(results[0].Two.UUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}

func (s *CursorSuite) TestCursor_PreloadJoinsDesc() {
	limit := TestPageSize

	steps := TestDataLen / limit
	if TestDataLen%limit > 0 {
		steps++
	}

	results, next, prev := s.queryPreloadJoins(fmt.Sprintf("limit=%d&order=desc(id)&order=desc(created_at)&status[]=hold&status[]=stop", limit))

	s.Equal(len(results), limit)
	s.Empty(prev)
	s.NotEmpty(next)

	// move forward
	step := 0
	for {
		results, next, prev = s.queryPreloadJoins("cursor=" + next)
		s.NotNil(results)
		if results == nil {
			break
		}
		step++

		if next != "" {
			s.Equal(len(results), limit)
			s.NotEmpty(results[0].UUID)
			s.NotEmpty(results[0].Two.UUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), TestDataLen-step*limit)
			s.Empty(next)
			break
		}
	}

	// move backward
	for {
		results, next, prev = s.queryPreloadJoins("cursor=" + prev)
		s.NotNil(results)
		if results == nil {
			break
		}
		step--

		if prev != "" {
			s.Equal(len(results), limit)
			s.NotEmpty(results[0].UUID)
			s.NotEmpty(results[0].Two.UUID)
			s.NotEmpty(prev)
			s.NotEmpty(next)
		} else {
			s.Equal(len(results), limit)
			s.Empty(prev)
			break
		}
	}
}
