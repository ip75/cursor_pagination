package pagination

import (
	"errors"
	"fmt"

	"gorm.io/gorm"
)

// FilterClauseConverter создаёт условие WHERE для различных источников данных
type FilterClauseConverter struct {
	filters map[string][]string
}

// NewFilterClauseConverter конструктор для FilterClauseConverter
func NewFilterClauseConverter(filters map[string][]string) (*FilterClauseConverter, error) {
	return &FilterClauseConverter{
		filters: filters,
	}, nil
}

// ToGORM создаёт условия фильтрации для GORM'a
func (c FilterClauseConverter) ToGORM(db *gorm.DB) (*gorm.DB, error) {
	if len(c.filters) == 0 {
		return db, nil
	}

	for column, conditions := range c.filters {
		for _, cond := range conditions {
			m := newFilterModificator()
			if err := m.Parse(cond); err != nil {
				if errors.Is(err, errModificatorIsEmpty) {
					db = db.Where(fmt.Sprintf("%s = ?", column), cond)
				}
				continue
			}

			if err := m.Validate(); err != nil {
				continue
			}

			// Составляем условие исходя из оператора поиска
			switch m.operator {
			case filterGt:
				db = db.Where(fmt.Sprintf("%s > ?", column), m.arg)
			case filterGe:
				db = db.Where(fmt.Sprintf("%s >= ?", column), m.arg)
			case filterLt:
				db = db.Where(fmt.Sprintf("%s < ?", column), m.arg)
			case filterLe:
				db = db.Where(fmt.Sprintf("%s <= ?", column), m.arg)
			case filterLike:
				db = db.Where(fmt.Sprintf("%s like ?", column), "%"+m.arg+"%")
			}
		}
	}

	return db, nil
}
