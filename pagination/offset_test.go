package pagination

import (
	"math"
	"math/rand"
	"net/url"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOffsetToken_ParseQuery(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewOffsetToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, c.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, c.Order[i])
		}

		// Проверка фильтров поиска
		assert.Equal(t, filters, c.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, c.Offset)
	})

	t.Run("ok - wrong order", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		expectedOrders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		orders := []string{
			"asc(id)",
			"desc(create_time)",
			"somewrongmodificator(id)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewOffsetToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, c.Limit)

		// Проверка сортировки
		assert.Equal(t, len(expectedOrders), len(c.Order))
		assert.Equal(t, expectedOrders, c.Order)

		// Проверка фильтров поиска
		assert.Equal(t, filters, c.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, c.Offset)
	})

	t.Run("ok - wrong filter", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		expectedFilters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		filters := map[string][]string{
			"age":   {"30", "gt(45)", ""},
			"name":  {""},
			"count": {"wrongmodificator(1)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		c := NewOffsetToken()
		err = c.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, c.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, c.Order[i])
		}

		// Проверка поискового фильтра
		assert.Equal(t, len(expectedFilters), len(c.Filter))
		assert.Equal(t, expectedFilters, c.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, c.Offset)
	})

	t.Run("ok - request with cursor", func(t *testing.T) {
		cursor := "eyJmaWx0ZXIiOnsiYWdlIjpbIjMwIiwiZ3QoNDUpIl19LCJvcmRlciI6WyJhc2MoaWQpIiwiZGVzYyhjcmVhdGVfdGltZSkiXSwibGltaXQiOjEwLCJvZmZzZXQiOjQwfQo="
		rawUrl := "https://example.com/api/v1/users?cursor=" + cursor

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		assert.NotEmpty(t, token.Limit)
		assert.NotEmpty(t, token.Filter)
		assert.NotEmpty(t, token.Order)
	})
}

func TestOffsetToken_ParseToken(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GenerateToken(page)
		assert.NoError(t, err)
		assert.True(t, len(encodedToken) > 0)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, token.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, token.Order[i])
		}

		// Проверка фильтров поиска
		assert.Equal(t, filters, token.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, token.Offset)
	})

	t.Run("ok - wrong order", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		expectedOrders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		orders := []string{
			"asc(id)",
			"desc(create_time)",
			"somewrongmodificator(id)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		filters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GenerateToken(page)
		assert.NoError(t, err)
		assert.True(t, len(encodedToken) > 0)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, token.Limit)

		// Проверка сортировки
		assert.Equal(t, len(expectedOrders), len(parsedToken.Order))
		assert.Equal(t, expectedOrders, parsedToken.Order)

		// Проверка фильтров поиска
		assert.Equal(t, filters, token.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, token.Offset)
	})

	t.Run("ok - wrong filter", func(t *testing.T) {
		// Подготовка тестового URL
		limit := uint64(10)
		page := uint64(1)

		orders := []string{
			"asc(id)",
			"desc(create_time)",
		}
		encodedOrder := url.Values{}
		for _, order := range orders {
			encodedOrder.Add("order", order)
		}

		expectedFilters := map[string][]string{
			"age": {"30", "gt(45)"},
		}
		filters := map[string][]string{
			"age":   {"30", "gt(45)", ""},
			"name":  {""},
			"count": {"wrongmodificator(1)"},
		}
		encodedFilters := url.Values{}
		for key, filter := range filters {
			for _, f := range filter {
				encodedFilters.Add(key, f)
			}
		}

		rawUrl := "https://example.com/api/v1/users?" +
			encodedOrder.Encode() + "&" +
			encodedFilters.Encode() + "&" +
			"limit=" + strconv.FormatUint(limit, 10) + "&" +
			"page=" + strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GenerateToken(page)
		assert.NoError(t, err)
		assert.True(t, len(encodedToken) > 0)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		// Проверка лимита
		assert.Equal(t, limit, token.Limit)

		// Проверка сортировки
		for i, order := range orders {
			assert.Equal(t, order, token.Order[i])
		}

		// Проверка поискового фильтра
		assert.Equal(t, len(expectedFilters), len(parsedToken.Filter))
		assert.Equal(t, expectedFilters, parsedToken.Filter)

		// Проверка смещения
		assert.Equal(t, (page-1)*limit, token.Offset)
	})
}

func TestOffsetToken_GenerateToken(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=1"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		page := uint64(5)
		encodedToken, err := token.GenerateToken(page)
		assert.NoError(t, err)
		assert.True(t, len(encodedToken) > 0)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedToken.Filter)
		assert.NotEmpty(t, parsedToken.Order)
		assert.NotEmpty(t, parsedToken.Limit)
		assert.Equal(t, token.calculateOffset(page), parsedToken.Offset)
	})

	t.Run("ok - page = max_int", func(t *testing.T) {
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=1"

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		page := uint64(math.MaxUint64)
		encodedToken, err := token.GenerateToken(page)
		assert.NoError(t, err)
		assert.True(t, len(encodedToken) > 0)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedToken.Filter)
		assert.NotEmpty(t, parsedToken.Order)
		assert.NotEmpty(t, parsedToken.Limit)
		assert.Equal(t, token.calculateOffset(page), parsedToken.Offset)
	})
}

func TestOffsetToken_GeneratePrevToken(t *testing.T) {
	t.Run("ok - 0 page", func(t *testing.T) {
		page := 0
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=" +
			strconv.Itoa(page)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GeneratePrevToken()
		assert.NoError(t, err)
		assert.Empty(t, encodedToken)
	})

	t.Run("ok - 1 page", func(t *testing.T) {
		page := 1
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=" +
			strconv.Itoa(page)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GeneratePrevToken()
		assert.NoError(t, err)
		assert.Empty(t, encodedToken)
	})

	t.Run("ok - N page", func(t *testing.T) {
		page := uint64(rand.Int63n(10000) + 1)
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=" +
			strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GeneratePrevToken()
		assert.NoError(t, err)
		assert.NotEmpty(t, encodedToken)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedToken.Filter)
		assert.NotEmpty(t, parsedToken.Order)
		assert.NotEmpty(t, parsedToken.Limit)
	})
}

func TestOffsetToken_GenerateNextToken(t *testing.T) {
	t.Run("ok - 0 page", func(t *testing.T) {
		page := 0
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=" +
			strconv.Itoa(page)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GenerateNextToken()
		assert.NoError(t, err)
		assert.NotEmpty(t, encodedToken)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedToken.Filter)
		assert.NotEmpty(t, parsedToken.Order)
		assert.NotEmpty(t, parsedToken.Limit)
		assert.True(t, token.Offset < parsedToken.Offset)
	})

	t.Run("ok - N page", func(t *testing.T) {
		page := uint64(rand.Int63n(10000) + 1)
		rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=" +
			strconv.FormatUint(page, 10)

		u, err := url.Parse(rawUrl)
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewOffsetToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Генерируем токен
		encodedToken, err := token.GenerateNextToken()
		assert.NoError(t, err)
		assert.NotEmpty(t, encodedToken)

		// Парсинг токена
		parsedToken := NewOffsetToken()
		err = parsedToken.ParseToken(encodedToken)
		assert.NoError(t, err)

		assert.NotEmpty(t, parsedToken.Filter)
		assert.NotEmpty(t, parsedToken.Order)
		assert.NotEmpty(t, parsedToken.Limit)
		assert.True(t, token.Offset < parsedToken.Offset)
	})
}
