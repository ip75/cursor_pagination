package pagination

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func TestNewFilterClauseConverter(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		type testCase struct {
			input map[string][]string
		}

		tests := []testCase{
			{
				input: map[string][]string{
					"age": {"30", "gt(45)"},
				},
			},

			{
				input: map[string][]string{
					"last_name": {"ivanov"},
				},
			},
			{
				input: map[string][]string{},
			},
			{
				input: nil,
			},
		}

		for _, tt := range tests {
			converter, err := NewFilterClauseConverter(tt.input)
			assert.NoError(t, err)
			assert.NotNil(t, converter)
		}
	})
}

func TestFilterClauseConverter_ToGORM(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		type testCase struct {
			input map[string][]string
			want  string
		}

		tests := []testCase{
			{
				input: map[string][]string{
					"last_name": {"ivanov"},
					"age":       {"gt(45)", "ge(100)"},
					"weight":    {"lt(100)", "le(999)"},
					"title":     {"like(some text)"},
				},
				want: "last_name = ? age > ? age >= ? weight < ? weight <= ? title like ?",
			},
			{
				input: map[string][]string{
					"last_name": {"ivanov"},
				},
				want: "last_name = ?",
			},
			{
				input: map[string][]string{},
				want:  "",
			},
			{
				input: nil,
				want:  "",
			},
		}

		type testData struct {
			LastName string
			Age      int
			Weight   int
			Title    string
		}

		// Создаём соединение с БД
		db, err := gorm.Open(sqlite.Open(":memory:"), &gorm.Config{})
		assert.NoError(t, err)
		rawDB := *db

		err = db.Migrator().CreateTable(&testData{})
		assert.NoError(t, err)

		// Создаём тестовую запись
		err = db.Create(testData{
			LastName: "ivanov",
			Age:      100,
			Weight:   99,
			Title:    "Title with some text",
		}).Error
		assert.NoError(t, err)

		for _, tt := range tests {
			// Создаём преобразователь
			converter, err := NewFilterClauseConverter(tt.input)
			assert.NoError(t, err)
			assert.NotNil(t, converter)

			// Создаём условие для GORM'a
			db, err = converter.ToGORM(db)
			assert.NoError(t, err)

			for _, cls := range db.Statement.Clauses {
				if c, ok := cls.Expression.(clause.Where); ok {
					for _, expr := range c.Exprs {
						if e, ok := expr.(clause.Expr); ok {
							assert.Equal(t, 1, strings.Count(tt.want, e.SQL))
						}
					}
				}
			}

			// Проверяем выборку из БД
			var res testData
			err = db.First(&res).Error
			assert.NoError(t, err)
			assert.NotEmpty(t, res)

			// Сбрасываем параметры фильтрации
			db = &rawDB
		}
	})
}
