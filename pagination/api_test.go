package pagination

import (
	"encoding/json"
	"math"
	"net/url"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCursorAPIPResponse(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		u, err := url.Parse(generateUrlForCursorToken())
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Создаём структуру с пагинацией
		prevData := map[string]string{
			"prev-key": "prev-value",
		}

		nextData := map[string]string{
			"next-key": "next-value",
		}

		// Создаём новый объект пагинации с токенами типа Cursor
		resp, err := NewCursorAPIPResponse(token, prevData, nextData)
		assert.NoError(t, err)

		assert.NotEmpty(t, resp.Pagination.Cursors.Prev)
		assert.NotEmpty(t, resp.Pagination.Cursors.Next)

		// Преобразуем объект пагинации в JSON
		bts, err := json.Marshal(&resp)
		assert.NoError(t, err)
		assert.NotEmpty(t, bts)
	})

	t.Run("ok - empty prev", func(t *testing.T) {
		u, err := url.Parse(generateUrlForCursorToken())
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Создаём структуру с пагинацией
		nextData := map[string]string{
			"next-key": "next-value",
		}

		// Создаём новый объект пагинации с токенами типа Cursor
		resp, err := NewCursorAPIPResponse(token, nil, nextData)
		assert.NoError(t, err)

		assert.Empty(t, resp.Pagination.Cursors.Prev)
		assert.NotEmpty(t, resp.Pagination.Cursors.Next)
	})

	t.Run("ok - empty next", func(t *testing.T) {
		u, err := url.Parse(generateUrlForCursorToken())
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Создаём структуру с пагинацией
		prevData := map[string]string{
			"prev-key": "prev-value",
		}

		// Создаём новый объект пагинации с токенами типа Cursor
		resp, err := NewCursorAPIPResponse(token, prevData, nil)
		assert.NoError(t, err)

		assert.NotEmpty(t, resp.Pagination.Cursors.Prev)
		assert.Empty(t, resp.Pagination.Cursors.Next)
	})

	t.Run("error - empty prev and next data", func(t *testing.T) {
		u, err := url.Parse(generateUrlForCursorToken())
		assert.NoError(t, err)

		// Разбираем запрос в структуру курсора
		token := NewCursorToken()
		err = token.ParseQuery(u.RawQuery)
		assert.NoError(t, err)

		// Создаём новый объект пагинации с токенами типа Cursor
		resp, err := NewCursorAPIPResponse(token, nil, nil)
		assert.Error(t, err)
		assert.Empty(t, resp)
	})

}

func TestNewOffsetAPIResponse(t *testing.T) {
	rawUrl := "https://example.com/api/v1/users?order=asc%28id%29&order=desc%28create_time%29&age=30&age=gt%2845%29&limit=10&page=1"

	u, err := url.Parse(rawUrl)
	assert.NoError(t, err)

	// Разбираем запрос в структуру курсора
	token := NewOffsetToken()
	err = token.ParseQuery(u.RawQuery)
	assert.NoError(t, err)

	// Создаём новый объект пагинации с токенами типа Offset
	countElements := uint64(98)
	resp, err := NewOffsetAPIResponse(token, countElements)
	assert.NoError(t, err)

	totalPages := int(math.Ceil(float64(countElements) / float64(token.Limit)))
	assert.NotEmpty(t, resp.Pagination.Cursors)
	assert.True(t, totalPages == len(resp.Pagination.Cursors))
}

func generateUrlForCursorToken() string {
	// Подготовка тестового URL
	limit := uint64(10)

	orders := []string{
		"asc(id)",
		"desc(create_time)",
	}
	encodedOrder := url.Values{}
	for _, order := range orders {
		encodedOrder.Add("order", order)
	}

	filters := map[string][]string{
		"age": {"30", "gt(45)"},
	}
	encodedFilters := url.Values{}
	for key, filter := range filters {
		for _, f := range filter {
			encodedFilters.Add(key, f)
		}
	}

	return "https://example.com/api/v1/users?" + encodedOrder.Encode() + "&" + encodedFilters.Encode() + "&limit=" + strconv.FormatUint(limit, 10)
}
